# TASK MANAGER

Training project.
A simple console application for manager task lists.

## DEVELOPER INFO

**NAME**: Alfred Karimov

**EMAIL**: alfred@karimov.ru

**EMAIL**: alfred_test@karimov.ru

## SOFTWARE

**JAVA**: OPENJDK 1.8

**OS**: WINDOWS 10 (1809)

## HARDWARE

**CPU**: i5 10 gen

**RAM**: 16GB

**SSD**: 256GB

## BUILD PROGRAM

```
mvn clean install
```

## RUN PROGRAM

```
java -jar ./t1-task-manager.jar
```
