package ru.t1.karimov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import ru.t1.karimov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.karimov.tm.api.endpoint.IUserEndpoint;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.dto.request.user.*;
import ru.t1.karimov.tm.dto.response.user.*;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.marker.SoapCategory;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.service.PropertyService;

import static org.junit.Assert.*;

@Category(SoapCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @Nullable
    private String adminToken;

    @Nullable
    private String testToken;

    @Before
    public void initTest() throws AbstractException {
        @NotNull final UserLoginResponse adminResponse = authEndpoint.login(new UserLoginRequest("admin", "admin"));
        adminToken = adminResponse.getToken();
        @NotNull final UserLoginResponse testResponse = authEndpoint.login(new UserLoginRequest("test", "test"));
        testToken = testResponse.getToken();
    }

    @Test
    public void testChangePassword() throws AbstractException {
        assertNotNull(testToken);
        @NotNull final UserChangePasswordRequest changePasswordRequest = new UserChangePasswordRequest();
        changePasswordRequest.setToken(testToken);
        changePasswordRequest.setPassword("newPassword");
        @NotNull final UserChangePasswordResponse changePasswordResponse = userEndpoint.changeUserPassword(
                changePasswordRequest
        );
        assertNotNull(changePasswordResponse);
        assertThrows(
                Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test", "test"))
        );
        changePasswordRequest.setPassword("test");
        @NotNull final UserChangePasswordResponse changePasswordBackResponse = userEndpoint.changeUserPassword(
                changePasswordRequest
        );
        assertNotNull(changePasswordBackResponse);
    }

    @Test
    public void testLock() throws AbstractException {
        assertNotNull(testToken);
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(testToken);
        assertNotNull(authEndpoint.logout(logoutRequest));

        @NotNull final UserLockRequest lockRequest = new UserLockRequest(adminToken);
        lockRequest.setLogin("test");
        assertNotNull(userEndpoint.lockUser(lockRequest));

        assertThrows(
                Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test", "test"))
        );
        @NotNull final UserUnlockRequest unlockRequest = new UserUnlockRequest(adminToken);
        unlockRequest.setLogin("test");
        userEndpoint.unlockUser(unlockRequest);
    }

    @Test
    public void testRegistryUser() throws AbstractException {
        @NotNull final UserRegistryRequest adminRegistryRequest = new UserRegistryRequest(adminToken);
        adminRegistryRequest.setLogin("tom");
        adminRegistryRequest.setPassword("tom");
        adminRegistryRequest.setEmail("tom@tst.ru");
        @NotNull final UserRegistryResponse response = userEndpoint.registryUser(adminRegistryRequest);
        assertNotNull(response);
        @Nullable final User user = response.getUser();
        assertNotNull(user);
        @Nullable final String userLogin = user.getLogin();
        assertNotNull(userLogin);
        assertEquals(adminRegistryRequest.getLogin(), userLogin);

        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin("tom");
        userLoginRequest.setPassword("tom");
        assertNotNull(authEndpoint.login(userLoginRequest));
    }

    @Test
    public void testRemoveUser() throws AbstractException {
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin("user");
        userLoginRequest.setPassword("user");
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(userLoginRequest);
        @Nullable final String userToken = userLoginResponse.getToken();
        assertNotNull(userToken);
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(userToken);
        assertNotNull(authEndpoint.logout(logoutRequest));

        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken);
        removeRequest.setLogin("user");
        assertNotNull(userEndpoint.removeUser(removeRequest));
        assertThrows(Exception.class, () -> authEndpoint.login(userLoginRequest));

        @NotNull final UserRegistryRequest adminRegistryRequest = new UserRegistryRequest(adminToken);
        adminRegistryRequest.setLogin("user");
        adminRegistryRequest.setPassword("user");
        adminRegistryRequest.setEmail("user@tst.ru");
        @NotNull final UserRegistryResponse userRegistryResponse = userEndpoint.registryUser(adminRegistryRequest);
        assertNotNull(userRegistryResponse);
    }

    @Test
    public void testUnlockUser() throws AbstractException {
        assertNotNull(testToken);
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(testToken);
        assertNotNull(authEndpoint.logout(logoutRequest));
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest("test", "test");
        @NotNull final UserLockRequest lockRequest = new UserLockRequest(adminToken);
        lockRequest.setLogin("test");
        assertNotNull(userEndpoint.lockUser(lockRequest));
        assertThrows(
                Exception.class,
                () -> authEndpoint.login(userLoginRequest)
        );

        @NotNull final UserUnlockRequest unlockRequest = new UserUnlockRequest(adminToken);
        unlockRequest.setLogin("test");
        assertNotNull(userEndpoint.unlockUser(unlockRequest));
        @NotNull final UserLoginResponse response = authEndpoint.login((userLoginRequest));
        assertNotNull(response);
    }

    @Test
    public void testUpdateUserProfile() throws AbstractException {
         assertNotNull(testToken);
        @NotNull final UserProfileResponse userProfileResponse = authEndpoint.profile(new UserProfileRequest(testToken));
        assertNotNull(userProfileResponse);
        @Nullable User user = userProfileResponse.getUser();
        assertNotNull(user);

        @NotNull final UserUpdateProfileRequest updateProfileRequest = new UserUpdateProfileRequest(testToken);
        updateProfileRequest.setFirstName("firstName");
        updateProfileRequest.setLastName("lastName");
        updateProfileRequest.setMiddleName("middleName");
        @NotNull final UserUpdateProfileResponse updateProfileResponse = userEndpoint.updateUserProfile(
                updateProfileRequest
        );
        assertNotNull(updateProfileResponse);
        user = updateProfileResponse.getUser();
        assertNotNull(user);
        assertEquals("lastName", user.getLastName());
        assertEquals("firstName", user.getFirstName());
        assertEquals("middleName", user.getMiddleName());
    }

}
