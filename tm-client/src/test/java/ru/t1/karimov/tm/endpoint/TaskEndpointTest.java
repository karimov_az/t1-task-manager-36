package ru.t1.karimov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.karimov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.karimov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.dto.request.task.*;
import ru.t1.karimov.tm.dto.request.user.UserLoginRequest;
import ru.t1.karimov.tm.dto.response.task.*;
import ru.t1.karimov.tm.dto.response.user.UserLoginResponse;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.enumerated.TaskSort;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.marker.SoapCategory;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.service.PropertyService;

import java.util.List;

import static org.junit.Assert.*;

@Category(SoapCategory.class)
public class TaskEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @Nullable
    private String token;

    @Nullable
    private Task task;

    @Before
    public void initTest() throws AbstractException {
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(new UserLoginRequest("test", "test"));
        token = loginResponse.getToken();
        taskEndpoint.clearTask(new TaskClearRequest(token));
        @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest(token);
        createRequest.setName("Test Task 1");
        createRequest.setDescription("Test Description 1");
        @NotNull final TaskCreateResponse createResponse = taskEndpoint.createTask(createRequest);
        assertNotNull(createResponse);
        task = createResponse.getTask();
    }

    @After
    public void initEndTest() throws AbstractException {
        taskEndpoint.clearTask(new TaskClearRequest(token));
    }

    @Test
    public void testChangeTaskStatusById() throws AbstractException {
        assertNotNull(task);
        @Nullable final String taskId = task.getId();
        @NotNull final Status newStatus = Status.IN_PROGRESS;

        @NotNull final TaskChangeStatusByIdRequest taskChangeStatusByIdRequest = new TaskChangeStatusByIdRequest(token);
        assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(taskChangeStatusByIdRequest)
        );
        taskChangeStatusByIdRequest.setId("");
        assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(taskChangeStatusByIdRequest)
        );
        taskChangeStatusByIdRequest.setStatus(null);
        assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(taskChangeStatusByIdRequest)
        );
        taskChangeStatusByIdRequest.setId(taskId);
        taskChangeStatusByIdRequest.setStatus(newStatus);
        @NotNull final TaskChangeStatusByIdResponse changeStatusByIdResponse = taskEndpoint.changeTaskStatusById(
                taskChangeStatusByIdRequest
        );
        @Nullable final Task actualTask = changeStatusByIdResponse.getTask();
        assertNotNull(actualTask);
        assertEquals(Status.IN_PROGRESS, actualTask.getStatus());
    }

    @Test
    public void testClearTask() throws AbstractException {
        @NotNull final TaskListRequest listRequest = new TaskListRequest(token, TaskSort.BY_CREATED);
        @Nullable List<Task> tasks = taskEndpoint.listTask(listRequest).getTaskList();
        assertNotNull(tasks);
        assertTrue(tasks.size() > 0);

        taskEndpoint.clearTask(new TaskClearRequest(token));
        tasks = taskEndpoint.listTask(listRequest).getTaskList();
        assertNull(tasks);
    }

    @Test
    public void testCreateTask() throws AbstractException {
        @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest(token);
        assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(createRequest)
        );
        createRequest.setName("");
        assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(createRequest)
        );
        createRequest.setName("Test Task 2");
        createRequest.setDescription("Test Description 2");
        @NotNull final TaskCreateResponse createResponse = taskEndpoint.createTask(createRequest);
        assertNotNull(createResponse);
        @Nullable Task actualTask = createResponse.getTask();
        assertNotNull(actualTask);
    }

    @Test
    public void testGetTaskById() throws AbstractException {
        assertNotNull(task);
        @NotNull final String id = task.getId();
        @NotNull final TaskGetByIdRequest taskGetByIdRequest = new TaskGetByIdRequest(token);
        assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskById(taskGetByIdRequest)
        );
        taskGetByIdRequest.setId("");
        assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskById(taskGetByIdRequest)
        );
        taskGetByIdRequest.setId("otherId");
        assertNull(taskEndpoint.getTaskById(taskGetByIdRequest).getTask());

        taskGetByIdRequest.setId(id);
        @NotNull final TaskGetByIdResponse taskGetByIdResponse = taskEndpoint.getTaskById(taskGetByIdRequest);
        assertNotNull(taskGetByIdResponse);
        @Nullable final Task actualTask = taskGetByIdResponse.getTask();
        assertNotNull(actualTask);
        assertEquals("Test Task 1", actualTask.getName());
    }

    @Test
    public void testGetTaskList() throws AbstractException {
        @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest(token);
        createRequest.setName("Task 2");
        createRequest.setDescription("Description 2");
        taskEndpoint.createTask(createRequest);
        createRequest.setName("Task 3");
        createRequest.setDescription("Description 3");
        taskEndpoint.createTask(createRequest);

        @NotNull final TaskListRequest taskListRequest = new TaskListRequest(token, TaskSort.BY_NAME);
        @Nullable final List<Task> tasks = taskEndpoint.listTask(taskListRequest).getTaskList();
        assertNotNull(tasks);
        assertEquals(3, tasks.size());
    }

    @Test
    public void testRemoveTaskById() throws AbstractException {
        @NotNull final TaskListRequest taskListRequest = new TaskListRequest(token, TaskSort.BY_NAME);
        @Nullable List<Task> tasks = taskEndpoint.listTask(taskListRequest).getTaskList();
        assertNotNull(tasks);
        assertEquals(1, tasks.size());
        @NotNull final String id = tasks.get(0).getId();

        @NotNull final TaskRemoveByIdRequest taskRemoveByIdRequest = new TaskRemoveByIdRequest(token);
        assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskById(taskRemoveByIdRequest)
        );
        taskRemoveByIdRequest.setId("");
        assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskById(taskRemoveByIdRequest)
        );
        taskRemoveByIdRequest.setId("otherId");
        assertNull(taskEndpoint.removeTaskById(taskRemoveByIdRequest).getTask());

        taskRemoveByIdRequest.setId(id);
        @NotNull final TaskRemoveByIdResponse taskRemoveByIdResponse =  taskEndpoint.removeTaskById(taskRemoveByIdRequest);
        assertNotNull(taskRemoveByIdResponse);
        tasks = taskEndpoint.listTask(taskListRequest).getTaskList();
        assertNull(tasks);
    }

    @Test
    public void testUpdateTaskById() throws AbstractException {
        assertNotNull(task);
        @Nullable String id = task.getId();

        @NotNull final TaskUpdateByIdRequest taskUpdateByIdRequest = new TaskUpdateByIdRequest(token);
        assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(taskUpdateByIdRequest)
        );
        taskUpdateByIdRequest.setId(id);
        taskUpdateByIdRequest.setName("New task name");
        taskUpdateByIdRequest.setDescription("New description");
        @NotNull final TaskUpdateByIdResponse taskUpdateByIdResponse = taskEndpoint.updateTaskById(
                taskUpdateByIdRequest
        );
        assertNotNull(taskUpdateByIdResponse);
        @Nullable Task actualTask = taskUpdateByIdResponse.getTask();
        assertNotNull(actualTask);
        assertEquals(id, actualTask.getId());
        assertEquals("New task name", actualTask.getName());
        assertEquals("New description", actualTask.getDescription());

        taskUpdateByIdRequest.setId("");
        assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(taskUpdateByIdRequest)
        );
        taskUpdateByIdRequest.setId("otherId");
        assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(taskUpdateByIdRequest)
        );
        taskUpdateByIdRequest.setId(id);
        taskUpdateByIdRequest.setName("");
        assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(taskUpdateByIdRequest)
        );
        taskUpdateByIdRequest.setId(null);
        assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(taskUpdateByIdRequest)
        );
    }

}
