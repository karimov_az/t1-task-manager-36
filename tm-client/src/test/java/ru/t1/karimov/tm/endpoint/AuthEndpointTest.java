package ru.t1.karimov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.karimov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.dto.request.user.UserLoginRequest;
import ru.t1.karimov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.karimov.tm.dto.request.user.UserProfileRequest;
import ru.t1.karimov.tm.dto.response.user.UserLoginResponse;
import ru.t1.karimov.tm.dto.response.user.UserLogoutResponse;
import ru.t1.karimov.tm.dto.response.user.UserProfileResponse;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.marker.SoapCategory;
import ru.t1.karimov.tm.service.PropertyService;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;

@Category(SoapCategory.class)
public class AuthEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @Test
    public void testLogin() throws AbstractException {
        assertThrows(Exception.class, () -> authEndpoint.login(
                new UserLoginRequest(null, null))
        );
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        assertNotNull(userLoginResponse);
        assertNotNull(userLoginResponse.getToken());
    }

    @Test
    public void testLogout() throws AbstractException {
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        assertNotNull(userLoginResponse);
        assertNotNull(userLoginResponse.getToken());
        @NotNull final UserLogoutResponse userLogoutResponse = authEndpoint.logout(
                new UserLogoutRequest(userLoginResponse.getToken())
        );
        assertNotNull(userLogoutResponse);
    }

    @Test
    public void testProfile() throws AbstractException {
        @Nullable final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        assertNotNull(userLoginResponse);
        assertNotNull(userLoginResponse.getToken());
        @Nullable final UserProfileResponse userProfileResponse = authEndpoint.profile(
                new UserProfileRequest(userLoginResponse.getToken())
        );
        assertNotNull(userProfileResponse);
        assertNotNull(userProfileResponse.getUser());
    }

}
