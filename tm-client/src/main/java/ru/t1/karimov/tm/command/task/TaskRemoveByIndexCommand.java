package ru.t1.karimov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.task.TaskRemoveByIndexRequest;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() -1;
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(getToken());
        request.setIndex(index);
        getTaskEndpoint().removeTaskByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-remove-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove task by index.";
    }

}
