package ru.t1.karimov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.user.UserUnlockRequest;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER UNLOCK");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        @NotNull final UserUnlockRequest request = new UserUnlockRequest(getToken());
        request.setLogin(login);
        getUserEndpoint().unlockUser(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "user-unlock";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User unlock.";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
