package ru.t1.karimov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.project.ProjectUpdateByIndexRequest;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() -1;
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(getToken());
        request.setIndex(index);
        request.setName(name);
        request.setDescription(description);
        getProjectEndpoint().updateProjectByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-update-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update project by index.";
    }

}
