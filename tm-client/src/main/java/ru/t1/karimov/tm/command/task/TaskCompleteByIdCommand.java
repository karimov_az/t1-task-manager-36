package ru.t1.karimov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.task.TaskCompleteByIdRequest;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(getToken());
        request.setId(id);
        getTaskEndpoint().completeTaskById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-complete-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Complete task by id.";
    }

}
