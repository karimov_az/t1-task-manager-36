package ru.t1.karimov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.dto.request.project.ProjectClearRequest;
import ru.t1.karimov.tm.exception.AbstractException;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[PROJECT CLEAR]");
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(getToken());
        getProjectEndpoint().clearProjects(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

}
