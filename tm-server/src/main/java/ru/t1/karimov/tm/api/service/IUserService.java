package ru.t1.karimov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.t1.karimov.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable String login, @Nullable String password) throws AbstractException;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email) throws AbstractException;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role) throws AbstractException;

    @Nullable
    User findByLogin(@Nullable String login) throws AbstractException;

    @Nullable
    User findByEmail(@Nullable String email) throws AbstractException;

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    void lockUserByLogin(@Nullable String login) throws AbstractException;

    @Nullable
    User removeOne(@Nullable User model) throws AbstractEntityNotFoundException;

    @Nullable
    User removeOneByLogin(@Nullable String login) throws AbstractException;

    @Nullable
    User removeOneByEmail(@Nullable String email) throws AbstractException;

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password) throws AbstractException;

    @NotNull
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    ) throws AbstractException;

    void unlockUserByLogin(@Nullable String login) throws AbstractException;

}
