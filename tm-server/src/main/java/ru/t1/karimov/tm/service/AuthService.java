package ru.t1.karimov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.service.IAuthService;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.api.service.ISessionService;
import ru.t1.karimov.tm.api.service.IUserService;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.field.LoginEmptyException;
import ru.t1.karimov.tm.exception.field.PasswordEmptyException;
import ru.t1.karimov.tm.exception.user.AccessDeniedException;
import ru.t1.karimov.tm.exception.user.AuthenticationException;
import ru.t1.karimov.tm.model.Session;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.util.CryptUtil;
import ru.t1.karimov.tm.util.HashUtil;

import java.util.Date;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ISessionService sessionService;

    public AuthService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserService userService,
            @NotNull ISessionService sessionService
    ) {
        this.propertyService = propertyService;
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final User user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final Session session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    private Session createSession(@NotNull final User user) throws AbstractException {
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        return sessionService.add(session);
    }

    @NotNull
    @Override
    public User check(@Nullable final String login, @Nullable final String password) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AuthenticationException();
        if (user.getLocked()) throw new AuthenticationException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AuthenticationException();
        if (!hash.equals(user.getPasswordHash())) throw new AuthenticationException();
        return user;
    }

    @Override
    public void invalidate(@Nullable final Session session) throws AbstractException {
        if (session == null) return;
        sessionService.removeOne(session);
    }

    @NotNull
    @Override
    public String login(@Nullable final String login, @Nullable final String password) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AuthenticationException();
        if (user.getLocked()) throw new AuthenticationException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AuthenticationException();
        if (!hash.equals(user.getPasswordHash())) throw new AuthenticationException();
        return getToken(user);
    }

    @NotNull
    @Override
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session validateToken(@Nullable final String token) {
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Session session = objectMapper.readValue(json, Session.class);
        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();
        return session;
    }

}
