package ru.t1.karimov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.api.service.IDomainService;
import ru.t1.karimov.tm.exception.AbstractException;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private IDomainService getDomainService() {
        return bootstrap.getDomainService();
    }

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void load() {
        if (!Files.exists(Paths.get(getDomainService().getBackupFile()))) return;
        getDomainService().loadDataBackup();
    }

    @SneakyThrows
    public void save() {
        getDomainService().saveDataBackup();
    }

    public void start() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop () {
        es.shutdown();
    }

}
