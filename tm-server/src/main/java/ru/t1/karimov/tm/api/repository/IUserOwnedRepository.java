package ru.t1.karimov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @NotNull
    M add(@NotNull String userId, @NotNull M model);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull String userId, @NotNull Integer index);

    boolean existsById(@NotNull String userId, @NotNull String id);

    int getSize(@NotNull String userId);

    @Nullable
    M removeOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    M removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    M removeOne(@NotNull String userId, @NotNull M model);

    void removeAll(@NotNull String userId);

}
