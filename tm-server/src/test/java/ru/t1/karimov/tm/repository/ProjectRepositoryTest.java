package ru.t1.karimov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.enumerated.ProjectSort;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.field.UserIdEmptyException;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.model.Project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@Category(UnitCategory.class)
public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectRepository projectRepository;

    @Before
    public void initRepository() throws AbstractException {
        projectList = new ArrayList<>();
        projectRepository = new ProjectRepository();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project " + i);
            project.setDescription("Description " + i);
            if (i < 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            projectList.add(project);
            projectRepository.add(project);
        }
    }

    @Test
    public void testAddProject() throws AbstractException {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Project newProject = new Project();
        projectRepository.add(newProject);
        assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testAdd() throws AbstractException {
        @NotNull final Project project = new Project();
        @NotNull final String userId = USER_ID_1;
        @NotNull final String name = "Test Project Name";
        @NotNull final String description = "Test Project Description";
        @NotNull final String id = project.getId();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projectRepository.add(USER_ID_1, project);
        assertEquals(NUMBER_OF_ENTRIES + 1, projectRepository.getSize());
        @Nullable final Project actualProject = projectRepository.findOneById(id);
        assertNotNull(actualProject);
        assertEquals(userId, actualProject.getUserId());
        assertEquals(name, actualProject.getName());
        assertEquals(description, actualProject.getDescription());
    }

    @Test
    public void testAddAll() {
        @NotNull List<Project> projectList = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final String userId = i < NUMBER_OF_ENTRIES / 2 ? USER_ID_2 : USER_ID_1;
            final int num = i + 1 + NUMBER_OF_ENTRIES;
            @NotNull final String name = "Test Project Name " + num;
            @NotNull final String description = "Test Project Description " + num;
            @NotNull final Project project = new Project(userId, name, description, Status.IN_PROGRESS);
            projectList.add(project);
        }
        projectRepository.add(projectList);
        assertEquals(NUMBER_OF_ENTRIES * 2, projectRepository.getSize());
    }

    @Test
    public void testClear() {
        projectRepository.removeAll();
        assertEquals(0, projectRepository.getSize());
    }

    @Test
    public void testClearForUserPositive() {
        @NotNull final List<Project> emptyList = new ArrayList<>();
        projectRepository.removeAll(USER_ID_1);
        assertEquals(emptyList, projectRepository.findAll(USER_ID_1));
        assertNotEquals(emptyList, projectRepository.findAll(USER_ID_2));
    }

    @Test
    public void testClearForUserNegative() {
        projectRepository.removeAll("Other_id");
        assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Project> actualProjectList = projectRepository.findAll();
        assertEquals(projectList, actualProjectList);
    }

    @Test
    public void testFindAllByUser() {
        @NotNull final List<Project> actualProjectList = projectRepository.findAll(USER_ID_1);
        assertEquals(projectList.subList(0, 5), actualProjectList);
    }

    @Test
    public void testFindAllSort() {
        for (@NotNull final ProjectSort sort : ProjectSort.values()) {
            @NotNull final List<Project> actualProjectList = projectRepository.findAll(sort.getComparator());
            projectList.sort(sort.getComparator());
            assertEquals(projectList, actualProjectList);
        }
    }

    @Test
    public void testFindAllSortByUser() {
        for (@NotNull final ProjectSort sort : ProjectSort.values()) {
            @NotNull final List<Project> actualProjectList = projectRepository.findAll(USER_ID_1, sort.getComparator());
            @NotNull final List<Project> projects = projectList.subList(0, 5);
            projects.sort(sort.getComparator());
            assertEquals(projects, actualProjectList);
        }
        for (@NotNull final ProjectSort sort : ProjectSort.values()) {
            @NotNull final List<Project> actualProjectList = projectRepository.findAll(USER_ID_2, sort.getComparator());
            @NotNull final List<Project> projects = projectList.subList(5, NUMBER_OF_ENTRIES);
            projects.sort(sort.getComparator());
            assertEquals(projects, actualProjectList);
        }
    }

    @Test
    public void testFindOneByIdPositive() throws AbstractException {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            assertEquals(project, projectRepository.findOneById(id));
        }
    }

    @Test
    public void testFindOneByIdNegative() throws AbstractException {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final String id = UUID.randomUUID().toString();
            assertNull(projectRepository.findOneById(id));
        }
    }

    @Test
    public void testFindOneByIdForUserPositive() throws AbstractException {
        for (@NotNull final Project project : projectList) {
            @Nullable final String userId = project.getUserId();
            if (userId == null) throw new UserIdEmptyException();
            @NotNull final String id = project.getId();
            assertNotNull(projectRepository.findOneById(userId, id));
            assertEquals(project, projectRepository.findOneById(userId, id));
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            assertNull(projectRepository.findOneById(USER_ID_1, UUID.randomUUID().toString()));
        }
    }

    @Test
    public void testFindOneByIndexPositive() throws AbstractException {
        int i = 0;
        for (@NotNull final Project project : projectList) {
            @Nullable final Project actualProject = projectRepository.findOneByIndex(i);
            assertNotNull(actualProject);
            assertEquals(project, actualProject);
            i++;
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testFindOneByIndexNegative() throws AbstractException {
        assertNull(projectRepository.findOneByIndex(NUMBER_OF_ENTRIES + 1));
    }

    @Test
    public void testFindOneByIndexForUserPositive() {
        int i = 0, j = 0;
        for (@NotNull final Project project : projectList) {
            @Nullable final String userId = project.getUserId();
            if (USER_ID_1.equals(userId)) {
                @Nullable final Project actualProject = projectRepository.findOneByIndex(USER_ID_1, i);
                assertNotNull(actualProject);
                assertEquals(project, actualProject);
                i++;
            }
            if (USER_ID_2.equals(userId)) {
                @Nullable final Project actualProject = projectRepository.findOneByIndex(USER_ID_2, j);
                assertNotNull(actualProject);
                assertEquals(project, actualProject);
                j++;
            }
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testFindOneByIndexForUserNegative() {
        @NotNull final String userId = UUID.randomUUID().toString();
        assertNull(projectRepository.findOneByIndex(userId, NUMBER_OF_ENTRIES + 1));
    }

    @Test
    public void testExistByIdPositive() {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            assertTrue(projectRepository.existsById(id));
        }
    }

    @Test
    public void testExistByIdNegative() {
        @NotNull final String id = UUID.randomUUID().toString();
        assertFalse(projectRepository.existsById(id));
    }

    @Test
    public void testExistByIdForUserPositive() throws AbstractException {
        for (@NotNull final Project project : projectList) {
            @Nullable final String userId = project.getUserId();
            if (userId == null) throw new UserIdEmptyException();
            @NotNull final String id = project.getId();
            assertTrue(projectRepository.existsById(userId, id));
        }
    }

    @Test
    public void testExistByIdForUserNegative() throws AbstractException {
        for (@NotNull final Project project : projectList) {
            @Nullable final String userId = project.getUserId();
            if (userId == null) throw new UserIdEmptyException();
            @NotNull final String id = UUID.randomUUID().toString();
            assertFalse(projectRepository.existsById(userId, id));
        }
    }

    @Test
    public void testGetSize() {
        final int projectRepositorySize = projectRepository.getSize();
        assertEquals(projectList.size(), projectRepositorySize);
        assertEquals(NUMBER_OF_ENTRIES, projectRepositorySize);
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final List<String> userList = Arrays.asList(USER_ID_1, USER_ID_2);
        final int expectedSize = NUMBER_OF_ENTRIES / userList.size();
        for (@NotNull final String userId : userList) {
            final int projectRepositorySize = projectRepository.getSize(userId);
            @Nullable final List<Project> projectsUser = projectList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            assertEquals(projectsUser.size(), projectRepositorySize);
            assertEquals(expectedSize, projectRepositorySize);
        }
    }

    @Test
    public void testRemovePositive() throws AbstractException {
        for (@NotNull final Project project : projectList) {
            projectRepository.removeOne(project);
            assertNull(projectRepository.findOneById(project.getId()));
        }
        assertNotEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testRemoveNegative() throws AbstractException {
        @NotNull final Project project = new Project();
        projectRepository.removeOne(project);
        assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testRemoveByIdPositive() throws AbstractException {
        for (@NotNull final Project project : projectList) {
            assertNotNull(projectRepository.removeOneById(project.getId()));
            assertNull(projectRepository.findOneById(project.getId()));
        }
    }

    @Test
    public void testRemoveByIdNegative() throws AbstractException {
        @NotNull final String idNotFromRepository = UUID.randomUUID().toString();;
        assertNull(projectRepository.removeOneById(idNotFromRepository));
    }

    @Test
    public void testRemoveByIdForUserPositive() {
        for (@NotNull final Project project : projectList) {
            @Nullable final String userId = project.getUserId();
            @NotNull final String id = project.getId();
            if (USER_ID_1.equals(userId)) {
                assertNotNull(projectRepository.removeOneById(USER_ID_1, id));
                assertNull(projectRepository.findOneById(USER_ID_1, id));
            } else {
                assertNull(projectRepository.removeOneById(USER_ID_1, id));
            }
        }
    }

    @Test
    public void testRemoveByIdForUserNegative() {
        @NotNull final String otherUserId = UUID.randomUUID().toString();
        @NotNull final String otherId = UUID.randomUUID().toString();
        assertNull(projectRepository.removeOneById(USER_ID_1, otherId));
        assertNull(projectRepository.removeOneById(USER_ID_2, otherId));
        assertNull(projectRepository.removeOneById(otherUserId,otherId));
    }

    @Test
    public void testRemoveByIndexPositive() throws AbstractException {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @Nullable final Project project = projectList.get(i);
            @Nullable final Project actualProject = projectRepository.removeOneByIndex(0);
            assertNotNull(actualProject);
            assertNotNull(project);
            assertEquals(project, actualProject);
        }
        assertEquals(0, projectRepository.getSize());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveByIndexNegative() throws AbstractException {
        final int index = NUMBER_OF_ENTRIES + 1;
        @Nullable final Project actualProject = projectRepository.removeOneByIndex(index);
        assertNull(actualProject);
    }

    @Test
    public void testRemoveByIndexForUserPositive() throws AbstractException {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = projectList.get(i);
            @Nullable final String userId = project.getUserId();
            if (USER_ID_1.equals(userId)) {
                @Nullable final Project actualProject = projectRepository.removeOneByIndex(USER_ID_1, 0);
                assertNotNull(actualProject);
                assertEquals(project, actualProject);
            }
            if (USER_ID_2.equals(userId)) {
                @Nullable final Project actualProject = projectRepository.removeOneByIndex(USER_ID_2, 0);
                assertNotNull(actualProject);
                assertEquals(project, actualProject);
            }
        }
        assertEquals(0, projectRepository.getSize());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveByIndexForUserNegative() {
        final int index = NUMBER_OF_ENTRIES + 1;
        @NotNull final String otherUserId = UUID.randomUUID().toString();
        assertNull(projectRepository.removeOneByIndex(otherUserId, index));
    }

}
