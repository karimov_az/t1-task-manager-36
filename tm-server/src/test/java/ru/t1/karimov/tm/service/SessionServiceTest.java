package ru.t1.karimov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.api.repository.ISessionRepository;
import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.api.repository.IUserRepository;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.api.service.ISessionService;
import ru.t1.karimov.tm.api.service.IUserService;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.exception.field.IdEmptyException;
import ru.t1.karimov.tm.exception.field.UserIdEmptyException;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.model.Session;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.repository.ProjectRepository;
import ru.t1.karimov.tm.repository.SessionRepository;
import ru.t1.karimov.tm.repository.TaskRepository;
import ru.t1.karimov.tm.repository.UserRepository;
import ru.t1.karimov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@Category(UnitCategory.class)
public class SessionServiceTest {

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private IUserService userService;

    @NotNull
    private final List<Session> sessionList = new ArrayList<>();

    @NotNull
    private final List<User> userList = new ArrayList<>();

    @Before
    public void initTest() {
        @NotNull final ISessionRepository sessionRepository = new SessionRepository();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);
        sessionService = new SessionService(sessionRepository);

        @NotNull final User userAdmin = new User();
        userAdmin.setLogin("admin");
        userAdmin.setPasswordHash(HashUtil.salt(propertyService, "admin"));
        userAdmin.setEmail("admin@tst.ru");
        userAdmin.setRole(Role.ADMIN);
        @NotNull final User userTest = new User();
        userTest.setLogin("test");
        userTest.setPasswordHash(HashUtil.salt(propertyService, "test"));
        userTest.setEmail("test@tst.ru");
        userTest.setRole(Role.USUAL);
        @NotNull final User userTom = new User();
        userTom.setLogin("tom");
        userTom.setPasswordHash(HashUtil.salt(propertyService, "tom"));
        userTom.setEmail("tom@tst.ru");
        userTom.setRole(Role.USUAL);
        @NotNull final User userSoul = new User();
        userSoul.setLogin("soul");
        userSoul.setPasswordHash(HashUtil.salt(propertyService, "soul"));
        userSoul.setEmail("soul@tst.ru");
        userSoul.setRole(Role.USUAL);
        userList.add(userAdmin);
        userList.add(userTest);
        userList.add(userTom);
        userList.add(userSoul);
        userService.add(userList);

        @NotNull final Session sessionAdmin = new Session();
        @NotNull final Session sessionTest = new Session();
        @Nullable final String adminUserId = userAdmin.getId();
        @Nullable final String testUserId = userTest.getId();
        sessionAdmin.setUserId(adminUserId);
        sessionTest.setUserId(testUserId);
        sessionList.add(sessionAdmin);
        sessionList.add(sessionTest);
        sessionList.add(new Session());
        sessionService.add(sessionList);
    }

    @After
    public void initClear() {
        sessionService.removeAll();
        userService.removeAll();
        sessionList.clear();
    }

    @Test
    public void testAdd() throws AbstractException {
        final int expectedSize = sessionList.size();
        assertEquals(expectedSize, sessionService.getSize());
        @NotNull final Session session = new Session();
        sessionService.add(session);
        assertEquals(expectedSize + 1, sessionService.getSize());
    }

    @Test
    public void testAddForUser() throws AbstractException {
        final int expectedSize = sessionList.size();
        int i = 0;
        assertEquals(expectedSize, sessionService.getSize());
        for (@NotNull final User user : userList) {
            @NotNull final Session session = new Session();
            assertNotNull(sessionService.add(user.getId(), session));
            i++;

            assertThrows(
                    UserIdEmptyException.class, () -> sessionService.add(null, session)
            );
            assertThrows(
                    UserIdEmptyException.class, () -> sessionService.add("", session)
            );
            assertNull(sessionService.add(user.getId(), null));
        }
        assertEquals(expectedSize + i, sessionService.getSize());
    }

    @Test
    public void testAddAll() {
        @NotNull final List<Session> sessions = new ArrayList<>();
        final int expectedSize = sessionList.size();
        assertEquals(expectedSize, sessionService.getSize());
        for (int i = 0; i < 50; i++) {
            sessions.add(new Session());
        }
        sessionService.add(sessions);
        assertEquals(expectedSize + sessions.size(), sessionService.getSize());
    }

    @Test
    public void testClear() {
        sessionService.removeAll();
        assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testClearForUser() throws AbstractException {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            sessionService.removeAll(userId);
            assertEquals(0, sessionService.getSize(userId));
        }
        @NotNull final List<Session> expectedSessions = sessionList.stream()
                .filter(m -> m.getUserId() == null)
                .collect(Collectors.toList());
        assertEquals(expectedSessions.size(), sessionService.getSize());
    }

    @Test
    public void testClearForUserNegative() throws AbstractException {
        sessionService.removeAll("otherUserId");
        assertEquals(sessionList.size(), sessionService.getSize());
        assertThrows(
                UserIdEmptyException.class, () -> sessionService.removeAll(null)
        );
        assertThrows(
                UserIdEmptyException.class, () -> sessionService.removeAll("")
        );
    }

    @Test
    public void testFindAll() {
        @NotNull List<Session> sessions = sessionService.findAll();
        assertEquals(sessionList.size(), sessions.size());
    }

    @Test
    public void testFindAllForUser() throws AbstractFieldException {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull List<Session> sessions = sessionService.findAll(userId);
            assertEquals(sessionService.getSize(userId), sessions.size());

            @Nullable final String nullUserId = null;
            assertThrows(
                    UserIdEmptyException.class, () -> sessionService.findAll(nullUserId)
            );
            assertThrows(
                    UserIdEmptyException.class, () -> sessionService.findAll("")
            );
        }
        @NotNull final List<Session> actualSession = sessionService.findAll("otherUserId");
        assertNotNull(actualSession);
        assertEquals(0, actualSession.size());
    }

    @Test
    public void testFindById() throws AbstractException {
        for (@NotNull final Session session : sessionList) {
            @NotNull final String id = session.getId();
            @Nullable final Session actualSession = sessionService.findOneById(id);
            assertNotNull(actualSession);
            assertEquals(session, actualSession);
        }
        assertThrows(
                IdEmptyException.class, () -> sessionService.findOneById(null)
        );
        assertThrows(
                IdEmptyException.class, () -> sessionService.findOneById("")
        );
        assertNull(sessionService.findOneById("otherId"));
    }

    @Test
    public void testFindByIdForUser() throws AbstractException {
        @NotNull final List<Session> sessions = sessionList.stream()
                .filter(m -> m.getUserId() != null)
                .collect(Collectors.toList());
        for (@NotNull final Session session : sessions) {
            @NotNull final String id = session.getId();
            @Nullable final String userId = session.getUserId();
            @Nullable final Session actualSession = sessionService.findOneById(userId, id);
            assertNotNull(actualSession);
            assertEquals(session, actualSession);

            assertThrows(
                    UserIdEmptyException.class, () -> sessionService.findOneById(null, id)
            );
            assertThrows(
                    UserIdEmptyException.class, () -> sessionService.findOneById("", id)
            );
            assertThrows(
                    IdEmptyException.class, () -> sessionService.findOneById(userId, null)
            );
            assertThrows(
                    IdEmptyException.class, () -> sessionService.findOneById(userId, null)
            );
            assertNull(sessionService.findOneById(userId, "otherId"));
            assertNull(sessionService.findOneById("otherId", id));
        }
        assertNull(sessionService.findOneById("otherId", "otherId"));
    }

    @Test
    public void testGetSize() {
        assertEquals(sessionList.size(), sessionService.getSize());
    }

    @Test
    public void testGetSizeForUser() throws AbstractFieldException {
        @NotNull final List<Session> sessions = sessionList.stream()
                .filter(m -> m.getUserId() != null)
                .collect(Collectors.toList());
        for (@NotNull final Session session : sessions) {
            @Nullable final String userId = session.getUserId();
            assertNotNull(userId);
            @NotNull final List<Session> expectedSessions = sessionList.stream()
                    .filter(m -> m.getUserId() != null)
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            assertEquals(expectedSessions.size(), sessionService.getSize(userId));
        }

        assertThrows(
                UserIdEmptyException.class, () -> sessionService.getSize(null)
        );
        assertThrows(
                UserIdEmptyException.class, () -> sessionService.getSize("")
        );
        assertEquals(0, sessionService.getSize("OtherUserId"));
    }

    @Test
    public void testExistsById() {
        for (@NotNull final Session session : sessionList) {
            @NotNull final String id = session.getId();
            assertTrue(sessionService.existsById(id));
        }
        assertFalse(sessionService.existsById("OtherId"));
    }

    @Test
    public void testExistsByIdForUser() throws AbstractException {
        @NotNull final List<Session> sessions = sessionList.stream()
                .filter(m -> m.getUserId() != null)
                .collect(Collectors.toList());
        for (@NotNull final Session session : sessions) {
            @NotNull final String id = session.getId();
            @Nullable final String userId = session.getUserId();
            assertNotNull(userId);
            assertTrue(sessionService.existsById(userId, id));

            assertThrows(
                    UserIdEmptyException.class, () -> sessionService.existsById("", id)
            );
            assertThrows(
                    UserIdEmptyException.class, () -> sessionService.existsById(null, id)
            );
            assertThrows(
                    IdEmptyException.class, () -> sessionService.existsById(userId, "")
            );
            assertThrows(
                    IdEmptyException.class, () -> sessionService.existsById(userId, null)
            );
            assertFalse(sessionService.existsById(userId, "otherId"));
            assertFalse(sessionService.existsById("OtherUserId", id));
        }
        assertFalse(sessionService.existsById("OtherUserId", "otherId"));
    }

    @Test
    public void testRemoveById() throws AbstractException {
        for (@NotNull final Session session : sessionList) {
            @NotNull final String id = session.getId();
            assertNotNull(sessionService.removeOneById(id));
        }
        assertEquals(0, sessionService.getSize());

        assertThrows(
                IdEmptyException.class, () -> sessionService.removeOneById(null)
        );
        assertThrows(
                IdEmptyException.class, () -> sessionService.removeOneById("")
        );
        assertNull(sessionService.removeOneById("otherId"));
    }

    @Test
    public void testRemoveByIdForUser() throws AbstractException {
        @NotNull final List<Session> sessions = sessionList.stream()
                .filter(m -> m.getUserId() != null)
                .collect(Collectors.toList());
        for (@NotNull final Session session : sessions) {
            @NotNull final String id = session.getId();
            @Nullable final String userId = session.getUserId();
            assertNotNull(sessionService.removeOneById(userId, id));
            assertNull(sessionService.findOneById(userId, id));

            assertThrows(
                    UserIdEmptyException.class, () -> sessionService.removeOneById(null, id)
            );
            assertThrows(
                    UserIdEmptyException.class, () -> sessionService.removeOneById("", id)
            );
            assertThrows(
                    IdEmptyException.class, () -> sessionService.removeOneById(userId, null)
            );
            assertThrows(
                    IdEmptyException.class, () -> sessionService.removeOneById(userId, null)
            );
            assertNull(sessionService.removeOneById(userId, "otherId"));
            assertNull(sessionService.removeOneById("otherId", id));
        }
        assertNull(sessionService.removeOneById("otherId", "otherId"));
    }

    @Test
    public void testRemoveOne() throws AbstractException {
        for (@NotNull final Session session : sessionList) assertNotNull(sessionService.removeOne(session));
        assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testRemoveOneForUser() throws AbstractException {
        @NotNull final List<Session> sessions = sessionList.stream()
                .filter(m -> m.getUserId() != null)
                .collect(Collectors.toList());
        for (@NotNull final Session session : sessions) {
            @Nullable final String userId = session.getUserId();
            @NotNull final String id = session.getId();
            assertNotNull(sessionService.removeOne(userId, session));
            assertNull(sessionService.findOneById(userId, id));

            assertThrows(
                    UserIdEmptyException.class, () -> sessionService.removeOne(null, session)
            );
            assertThrows(
                    UserIdEmptyException.class, () -> sessionService.removeOne("", session)
            );
            assertNull(sessionService.removeOne(userId, null));
        }
        @NotNull final List<Session> expectedSessions = sessionList.stream()
                .filter(m -> m.getUserId() == null)
                .collect(Collectors.toList());
        assertEquals(expectedSessions.size(), sessionService.getSize());
    }

}
