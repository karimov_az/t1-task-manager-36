package ru.t1.karimov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.karimov.tm.api.repository.ISessionRepository;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.model.Session;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@Category(UnitCategory.class)
public class SessionRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final int HALF_NUMBER_OF_ENTRIES = NUMBER_OF_ENTRIES / 2;

    private static final int DOUBLE_NUMBER_OF_ENTRIES = NUMBER_OF_ENTRIES * 2;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private ISessionRepository sessionRepository;

    @Before
    public void initRepository() throws AbstractException {
        sessionList = new ArrayList<>();
        sessionRepository = new SessionRepository();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            final boolean condition = i < NUMBER_OF_ENTRIES / 2;
            @NotNull final String userId = condition ? USER_ID_1 : USER_ID_2;
            @NotNull final Session session = new Session(userId, Role.USUAL);
            sessionRepository.add(session);
            sessionList.add(session);
        }
    }

    @Test
    public void testAdd() throws AbstractException {
        @NotNull final Session session = new Session(USER_ID_1, Role.USUAL);
        sessionRepository.add(session);
        assertEquals(NUMBER_OF_ENTRIES + 1, sessionRepository.getSize());
    }

    @Test
    public void testAddAll() {
        @NotNull List<Session> sessionList = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) sessionList.add(new Session());
        sessionRepository.add(sessionList);
        assertEquals(DOUBLE_NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testClear() {
        sessionRepository.removeAll();
        assertEquals(0, sessionRepository.getSize());
    }

    @Test
    public void testClearForUserPositive() {
        @NotNull final List<String> userList = Arrays.asList(USER_ID_1, USER_ID_2);
        for (@NotNull final String userId : userList) sessionRepository.removeAll(userId);
        assertEquals(0, sessionRepository.getSize());
    }

    @Test
    public void testClearForUserNegative() {
        @NotNull final String userId = UUID.randomUUID().toString();
        sessionRepository.removeAll(userId);
        assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Session> actualSessionList = sessionRepository.findAll();
        assertEquals(sessionList, actualSessionList);
    }

    @Test
    public void testFindByIdPositive() throws AbstractException {
        for (@NotNull final Session session : sessionList) {
            assertNotNull(sessionRepository.findOneById(session.getId()));
            assertEquals(session, sessionRepository.findOneById(session.getId()));
        }
    }

    @Test
    public void testFindByIdNegative() throws AbstractException {
        @NotNull final String id = UUID.randomUUID().toString();
        assertNull(sessionRepository.findOneById(id));
    }

    @Test
    public void testFindByIdForUserPositive() {
        for (@NotNull final Session session : sessionList) {
            @Nullable final String userId = session.getUserId();
            @NotNull final String id = session.getId();
            if (userId == null) continue;
            assertNotNull(sessionRepository.findOneById(userId, id));
            assertEquals(session, sessionRepository.findOneById(userId, id));
        }
    }

    @Test
    public void testFindByIdForUserNegative() {
        @NotNull final String id = UUID.randomUUID().toString();
        @NotNull final String userId = UUID.randomUUID().toString();
        assertNull(sessionRepository.findOneById(userId, id));
    }

    @Test
    public void testGetSize() {
        assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        assertEquals(HALF_NUMBER_OF_ENTRIES, sessionRepository.getSize(USER_ID_1));
    }

    @Test
    public void testRemovePositive() throws AbstractException {
        for (@NotNull final Session session : sessionList) {
            sessionRepository.removeOne(session);
            assertNull(sessionRepository.findOneById(session.getId()));
        }
        assertEquals(0, sessionRepository.getSize());
    }

    @Test
    public void testRemoveNegative() throws AbstractException {
        @NotNull final Session sessionNotFromRepository = new Session();
        sessionRepository.removeOne(sessionNotFromRepository);
        assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testRemoveByIdPositive() throws AbstractException {
        for (@NotNull final Session session : sessionList) {
            assertNotNull(sessionRepository.removeOneById(session.getId()));
            assertNull(sessionRepository.findOneById(session.getId()));
        }
        assertEquals(0, sessionRepository.getSize());
    }

    @Test
    public void testRemoveByIdNegative() throws AbstractException {
        @NotNull final String idNotFromRepository = UUID.randomUUID().toString();
        assertNull(sessionRepository.removeOneById(idNotFromRepository));
        assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testRemoveByIdForUserPositive() {
        for (@NotNull final Session session : sessionList) {
            @Nullable final String userId = session.getUserId();
            @NotNull final String id = session.getId();
            if (userId == null) continue;
            assertNotNull(sessionRepository.removeOneById(userId, id));
            assertNull(sessionRepository.findOneById(userId, id));
        }
        assertEquals(0, sessionRepository.getSize());
    }

    @Test
    public void testRemoveByIdForUserNegative() {
        @NotNull final String otherId = UUID.randomUUID().toString();
        @NotNull final String otherUserId = UUID.randomUUID().toString();
        assertNull(sessionRepository.removeOneById(otherUserId, otherId));
        assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

}
