package ru.t1.karimov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.karimov.tm.api.repository.IUserRepository;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@Category(UnitCategory.class)
public class UserRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final int DOUBLE_NUMBER_OF_ENTRIES = NUMBER_OF_ENTRIES * 2;

    @NotNull
    private List<User> userList;

    @NotNull
    private IUserRepository userRepository;

    @Before
    public void initRepository() throws AbstractException {
        userList = new ArrayList<>();
        userRepository = new UserRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("user" + i);
            user.setEmail("user" + i + "@tst.ru");
            user.setRole(Role.USUAL);
            userRepository.add(user);
            userList.add(user);
        }
    }

    @Test
    public void testAdd() throws AbstractException {
        userRepository.add(new User());
        assertEquals(NUMBER_OF_ENTRIES + 1, userRepository.getSize());
    }

    @Test
    public void testAddAll() {
        @NotNull List<User> userList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            userList.add(new User());
        }
        userRepository.add(userList);
        assertEquals(DOUBLE_NUMBER_OF_ENTRIES, userRepository.getSize());
    }

    @Test
    public void testClear() {
        userRepository.removeAll();
        assertEquals(0, userRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<User> actualUserList = userRepository.findAll();
        assertEquals(userList, actualUserList);
        assertEquals(NUMBER_OF_ENTRIES, actualUserList.size());
    }

    @Test
    public void testFindByIdPositive() throws AbstractException {
        for (@NotNull final User user : userList) {
            @NotNull final String id = user.getId();
            assertNotNull(userRepository.findOneById(id));
            assertEquals(user, userRepository.findOneById(id));
        }
    }

    @Test
    public void testFindByIdNegative() throws AbstractException {
        @NotNull final String id = UUID.randomUUID().toString();
        assertNull(userRepository.findOneById(id));
    }

    @Test
    public void testFindByLoginPositive() {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            if (login == null) continue;
            assertNotNull(userRepository.findByLogin(login));
            assertEquals(user, userRepository.findByLogin(login));
        }
    }

    @Test
    public void testFindByLoginNegative() {
        @NotNull final String login = "OtherLogin";
        assertNull(userRepository.findByLogin(login));
    }

    @Test
    public void testFindByEmailPositive() {
        for (@NotNull final User user : userList) {
            @Nullable final String email = user.getEmail();
            if (email == null) continue;
            assertNotNull(userRepository.findByEmail(email));
            assertEquals(user, userRepository.findByEmail(email));
        }
    }

    @Test
    public void testFindByEmailNegative() {
        @NotNull final String email = "OtherEmail";
        assertNull(userRepository.findByEmail(email));
    }

    @Test
    public void testGetSize() {
        assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
    }

    @Test
    public void testIsLoginExistPositive() {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            if (login == null) continue;
            assertTrue(userRepository.isLoginExist(login));
        }
    }

    @Test
    public void testIsLoginExistNegative() {
        @Nullable final String login = "OtherLogin";
        assertFalse(userRepository.isLoginExist(login));
    }

    @Test
    public void testisEmailExistPositive() {
        for (@NotNull final User user : userList) {
            @Nullable final String email = user.getEmail();
            if (email == null) continue;
            assertTrue(userRepository.isEmailExist(email));
        }
    }

    @Test
    public void testEmailExistNegative() {
        @Nullable final String email = "OtherEmail";
        assertFalse(userRepository.isEmailExist(email));
    }

}
