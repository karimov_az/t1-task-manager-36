package ru.t1.karimov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.api.repository.IUserRepository;
import ru.t1.karimov.tm.api.service.*;
import ru.t1.karimov.tm.enumerated.ProjectSort;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.karimov.tm.exception.field.*;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.repository.ProjectRepository;
import ru.t1.karimov.tm.repository.TaskRepository;
import ru.t1.karimov.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IUserService userService;

    @NotNull
    private IProjectTaskService projectTaskService;

    @NotNull
    private List<Task> taskList;

    @NotNull
    private List<User> userList;

    @Before
    public void initTest() throws AbstractException {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        projectService = new ProjectService(projectRepository);
        taskService = new TaskService(taskRepository);
        projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
        userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);
        projectList = new ArrayList<>();
        taskList = new ArrayList<>();
        userList = new ArrayList<>();

        @NotNull final User userAdmin = new User();
        userAdmin.setLogin("admin");
        userAdmin.setPasswordHash("admin");
        userAdmin.setEmail("admin@tst.ru");
        userAdmin.setRole(Role.ADMIN);
        @NotNull final User userTest = new User();
        userTest.setLogin("test");
        userTest.setPasswordHash("test");
        userTest.setEmail("test@tst.ru");
        userTest.setRole(Role.USUAL);
        @NotNull final String adminId = userAdmin.getId();
        @NotNull final String userId = userTest.getId();

        userService.add(userAdmin);
        userService.add(userTest);
        userList.add(userAdmin);
        userList.add(userTest);
        projectList.add(new Project(adminId, "Project 1", "Desc 1", Status.IN_PROGRESS));
        projectList.add(new Project(adminId, "Project 2", "Desc 2", Status.NOT_STARTED));
        projectList.add(new Project(userId, "Project 1", "Desc 1", Status.IN_PROGRESS));
        projectList.add(new Project(userId, "Project 2", "Desc 2", Status.IN_PROGRESS));
        projectList.add(new Project(userId, "Project 3", "Desc 3", Status.NOT_STARTED));
        taskList.add(new Task(adminId, "Task 1", "Desc 1", Status.NOT_STARTED, projectList.get(0).getId()));
        taskList.add(new Task(adminId, "Task 2", "Desc 2", Status.IN_PROGRESS, projectList.get(0).getId()));
        taskList.add(new Task(userId, "Task 1", "Desc 1", Status.IN_PROGRESS, projectList.get(4).getId()));
        taskList.add(new Task(userId, "Task 2", "Desc 2", Status.IN_PROGRESS, null));
        for (@NotNull final Project project : projectList) projectService.add(project);
        for (@NotNull final Task task : taskList) taskService.add(task);
    }

    @Test
    public void testAdd() throws AbstractException {
        @NotNull final Project project = new Project("test", "Project", "Description", Status.NOT_STARTED);
        projectService.add(project);
        final int expectedCount = projectList.size() + 1;
        assertEquals(expectedCount, projectService.getSize());
    }

    @Test
    public void testAddAll() {
        @NotNull List<Project> projects = new ArrayList<>();
        final int expectedCount = projectList.size();
        for (int i = 0; i < expectedCount; i++) {
            projects.add(new Project());
        }
        projectService.add(projects);
        assertEquals(expectedCount * 2, projectService.getSize());
    }

    @Test
    public void testCreate() throws AbstractException {
        int expectedSize = projectList.size();
        int i = 0;
        for(@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final String name = "Project " + user.getLogin();
            @NotNull final Project project = projectService.create(userId, name);
            assertNotNull(project);
            i++;
        }
        expectedSize = expectedSize + i;
        assertEquals(expectedSize, projectService.getSize());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateWithNullUser() throws AbstractException {
        assertNotNull(projectService.create(null, "Project name"));
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateWithNullName() throws AbstractException {
        assertNotNull(projectService.create("test", null));
    }

    @Test
    public void testCreateFull() throws AbstractException {
        int expectedSize = projectList.size();
        int i = 0;
        for(@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final String name = "Project " + user.getLogin();
            @NotNull final String description = "Description " + user.getLogin();
            @NotNull final Project project = projectService.create(userId, name, description);
            assertNotNull(project);
            i++;
        }
        expectedSize = expectedSize + i;
        assertEquals(expectedSize, projectService.getSize());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateFullWithNullUser() throws AbstractException {
        assertNotNull(projectService.create(null, "Project name", "Desc"));
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateFullWithNullName() throws AbstractException {
        assertNotNull(projectService.create("test", null, "Desc"));
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testCreateFullWithNullDesc() throws AbstractException {
        assertNotNull(projectService.create("test", "Project name", null));
    }

    @Test
    public void testClear() {
        projectService.removeAll();
        assertEquals(0, projectService.getSize());
    }

    @Test
    public void testClearForUserPositive() throws AbstractException {
        for(@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            projectService.removeAll(userId);
            assertEquals(0, projectService.getSize(userId));
        }
        assertEquals(0, projectService.getSize());
    }

    @Test
    public void testClearForUserNegative() throws AbstractException {
        @NotNull final String otherUserId = "OtherUserId";
        projectService.removeAll(otherUserId);
        assertEquals(projectList.size(), projectService.getSize());
    }

    @Test
    public void testChangeStatusByIdPositive() throws AbstractException {
        for (@NotNull final Project project : projectList) {
            @Nullable final String userId = project.getUserId();
            @NotNull final String id = project.getId();
            assertNotNull(projectService.changeProjectStatusById(userId, id, Status.COMPLETED));
            @Nullable final Project actualProject = projectService.findOneById(userId, id);
            assertNotNull(actualProject);
            @NotNull final Status actualStatus = actualProject.getStatus();
            assertEquals(Status.COMPLETED, actualStatus);
        }
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testChangeStatusByIdNegative() throws AbstractException {
        @NotNull final String userId = "otherUserId";
        @NotNull final String id = "otherId";
        assertNotNull(projectService.changeProjectStatusById(userId, id, Status.COMPLETED));
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeStatusByIdNullStatus() throws AbstractException {
        @Nullable final String userId = projectList.get(2).getUserId();
        @NotNull final String id = projectList.get(2).getId();
        assertNotNull(projectService.changeProjectStatusById(userId, id, null));
    }

    @Test
    public void testChangeStatusByIndexPositive() throws AbstractException {
        for(@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Project> projects = projectList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (int i = 0; i < projects.size(); i++) {
                assertNotNull(projectService.changeProjectStatusByIndex(userId, i, Status.COMPLETED));
                @Nullable final Project actualProject = projectService.findOneByIndex(userId, i);
                assertNotNull(actualProject);
                @NotNull final Status actualStatus = actualProject.getStatus();
                assertEquals(Status.COMPLETED, actualStatus);
            }
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeStatusByIndexNegative() throws AbstractException {
        @NotNull final String userId = "otherUserId";
        final int index = projectList.size() + 1;
        assertNotNull(projectService.changeProjectStatusByIndex(userId, index, Status.COMPLETED));
        @Nullable final Project actualProject = projectService.findOneByIndex(userId, index);
        assertNull(actualProject);
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeStatusByIndexNullStatus() throws AbstractException {
        @NotNull final String userId = userList.get(1).getId();
        final int index = 0;
        assertNotNull(projectService.changeProjectStatusByIndex(userId, index, null));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Project> projects = projectService.findAll();
        assertNotNull(projects);
        assertEquals(projectList.size(), projects.size());
    }

    @Test
    public void testFindAllUser() throws AbstractException {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Project> actualProjects = projectService.findAll(userId);
            @NotNull final List<Project> expectedProjects = projectList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            assertEquals(expectedProjects, actualProjects);
        }
    }

    @Test
    public void testFindAllSort() {
        for (@NotNull final ProjectSort sort : ProjectSort.values()) {
            @NotNull final Comparator<Project> comparator = sort.getComparator();
            @NotNull final List<Project> actualProjects = projectService.findAll(comparator);
            assertNotNull(actualProjects);
            @NotNull final List<Project> expectedProjects = projectList.stream()
                    .sorted(comparator)
                    .collect(Collectors.toList());
            assertEquals(expectedProjects, actualProjects);
        }
    }

    @Test
    public void testFindAllForUserSort() throws AbstractException {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            for (@NotNull final ProjectSort sort : ProjectSort.values()) {
                @NotNull final Comparator<Project> comparator = sort.getComparator();
                @NotNull final List<Project> actualProjects = projectService.findAll(userId, comparator);
                assertNotNull(actualProjects);
                @NotNull final List<Project> expectedProjects = projectList.stream()
                        .filter(m -> userId.equals(m.getUserId()))
                        .sorted(comparator)
                        .collect(Collectors.toList());
                assertEquals(expectedProjects, actualProjects);
            }
        }
    }

    @Test
    public void testFindByIdPositive() throws AbstractException {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            assertEquals(project, projectService.findOneById(id));
        }
    }

    @Test
    public void testFindByIdNegative() throws AbstractException {
        @NotNull final String id = "OtherId";
        @Nullable final Project project = projectService.findOneById(id);
        assertNull(project);
    }

    @Test
    public void testFindByIdUserPositive() throws AbstractException {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            @Nullable final String userId = project.getUserId();
            assertEquals(project, projectService.findOneById(userId, id));
        }
    }

    @Test
    public void testFindByIdForUserNegative() throws AbstractException {
        @NotNull final String id = "OtherId";
        @NotNull final String userId = "OtherUserId";
        @Nullable final Project project = projectService.findOneById(userId, id);
        assertNull(project);
    }

    @Test
    public void testFindByIndexPositive() throws AbstractException {
        for (int i = 0; i < projectList.size(); i++) {
            @Nullable final Project actualProject = projectService.findOneByIndex(i);
            @Nullable final Project expectedProject = projectList.get(i);
            assertNotNull(actualProject);
            assertEquals(expectedProject, actualProject);
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexNegative() throws AbstractException {
        final int index = projectList.size() + 1;
        assertNull(projectService.findOneByIndex(index));
    }

    @Test
    public void testFindByIndexForUserPositive() throws AbstractException {
        for(@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Project> projects = projectList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (int i = 0; i < projects.size(); i++) {
                assertNotNull(projectService.findOneByIndex(userId, i));
                @Nullable final Project actualProject = projectService.findOneByIndex(userId, i);
                assertNotNull(actualProject);
                @Nullable final Project expectedProject = projects.get(i);
                assertEquals(expectedProject, actualProject);
            }
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexForUserNegative() throws AbstractException {
        @NotNull final String userId = "OtherUserId";
        final int index = projectList.size() + 1;
        assertNull(projectService.findOneByIndex(userId, index));
    }

    @Test
    public void testGetSize() {
        final int actualProjectSize = projectService.getSize();
        assertEquals(projectList.size(), actualProjectSize);
    }

    @Test
    public void testGetSizeUser() throws AbstractException {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            final int actualProjectSize = projectService.getSize(userId);
            @NotNull final List<Project> expectedProjects = projectList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            final int expectedProjectSize = expectedProjects.size();
            assertEquals(expectedProjectSize, actualProjectSize);
        }
    }

    @Test
    public void testGetSizeUserNegative() throws AbstractException {
        @NotNull final String userId = "OtherUserId";
        final int actualProjectSize = projectService.getSize(userId);
        assertEquals(0, actualProjectSize);
    }

    @Test
    public void testExistsById() {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            assertTrue(projectService.existsById(id));
        }
        assertFalse(projectService.existsById("OtherId"));
    }

    @Test
    public void testExistsByEmptyIdNegative() {
        assertFalse(projectService.existsById(""));
    }

    @Test
    public void testExistsByIdForUser() throws AbstractException {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            @Nullable final String userId = project.getUserId();
            assertTrue(projectService.existsById(userId, id));
        }
        assertFalse(projectService.existsById("OtherUserId","OtherId"));
    }

    @Test(expected = IdEmptyException.class)
    public void testExistsByIdForUserWithNullId() throws AbstractException {
        assertFalse(projectService.existsById(userList.get(0).getId(),null));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testExistsByIdForUserWithNullUserId() throws AbstractException {
        assertFalse(projectService.existsById(null, projectList.get(0).getId()));
    }

    @Test
    public void testRemovedAll() {
        projectService.removeAll();
        assertEquals(0, projectService.getSize());
    }

    @Test
    public void testRemovedAllForUserPositive() throws AbstractException {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            projectService.removeAll(userId);
            assertEquals(0, projectService.getSize(userId));
        }
        assertEquals(0, projectService.getSize());
    }

    @Test
    public void testRemovedAllForUserNegative() throws AbstractException {
        @NotNull final String userId = "OtherUserId";
        projectService.removeAll(userId);
        assertEquals(projectList.size(), projectService.getSize());
    }

    @Test
    public void testRemovedByIdPositive() throws AbstractException {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            assertNotNull(projectService.removeOneById(id));
        }
        assertEquals(0, projectService.getSize());
    }

    @Test
    public void testRemovedByIdNegative() throws AbstractException {
        @NotNull final String id = "OtherId";
        assertNull(projectService.removeOneById(id));
        assertEquals(projectList.size(), projectService.getSize());
    }

    @Test(expected = IdEmptyException.class)
    public void testRemovedByEmptyIdNegative() throws AbstractException {
        @NotNull final String id = "";
        assertNull(projectService.removeOneById(id));
        assertEquals(projectList.size(), projectService.getSize());
    }

    @Test
    public void testRemoveByIdForUserPositive() throws AbstractException {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Project> projects = projectList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (@NotNull final Project project : projects) {
                @NotNull final String id = project.getId();
                assertNotNull(projectService.removeOneById(userId, id));
            }
            assertEquals(0, projectService.getSize(userId));
        }
        assertEquals(0, projectService.getSize());
    }

    @Test
    public void testRemovedByIdForUserNegative() throws AbstractException {
        @NotNull final String id = "OtherId";
        @NotNull final String userId = "OtherUserId";
        assertNull(projectService.removeOneById(userId, id));
        assertEquals(projectList.size(), projectService.getSize());
    }

    @Test(expected = IdEmptyException.class)
    public void testRemovedByEmptyIdForUserNegative() throws AbstractException {
        @NotNull final String id = "";
        @NotNull final String userId = userList.get(0).getId();
        assertNull(projectService.removeOneById(userId, id));
    }

    @Test(expected = IdEmptyException.class)
    public void testRemovedByNullIdForUserNegative() throws AbstractException {
        @NotNull final String userId = userList.get(0).getId();
        assertNull(projectService.removeOneById(userId, null));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemovedByIdForUserWithNullUserIdNegative() throws AbstractException {
        @NotNull final String id = projectList.get(0).getId();
        assertNull(projectService.removeOneById(null, id));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemovedByIdForUserWithEmptyUserIdNegative() throws AbstractException {
        @NotNull final String id = projectList.get(0).getId();;
        @NotNull final String userId = "";
        assertNull(projectService.removeOneById(userId, id));
    }

    @Test
    public void testRemoveByIndexPositive() throws AbstractException {
        for (int i = 0; i < projectList.size(); i++) {
            assertNotNull(projectService.removeOneByIndex(0));
        }
        assertEquals(0, projectService.getSize());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIndexNegative() throws AbstractException {
        @NotNull final Integer index = projectList.size() + 1;
        assertNull(projectService.removeOneByIndex(index));
    }

    @Test
    public void testRemoveByIndexForUserPositive() throws AbstractException {
        for(@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Project> projects = projectList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (int i = 0; i < projects.size(); i++) {
                assertNotNull(projectService.removeOneByIndex(userId, 0));
            }
            assertEquals(0, projectService.getSize(userId));
        }
        assertEquals(0, projectService.getSize());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIndexForUserNegative() throws AbstractException {
        @NotNull final String userId = "otherUserId";
        @NotNull final Integer index = projectList.size() + 1;
        assertNull(projectService.removeOneByIndex(userId, index));
    }

    @Test
    public void testUpdateByIdPositive() throws AbstractException {
        for (@NotNull final Project project : projectList) {
            @Nullable final String userId = project.getUserId();
            if (userId == null) continue;
            @NotNull final String id = project.getId();
            @NotNull final String name = "Project " + id;
            @NotNull final String description = "Desc " + id;
            @Nullable final Project actualProject = projectService.updateProjectById(userId, id, name, description);
            assertEquals(name, actualProject.getName());
            assertEquals(description, actualProject.getDescription());
            assertEquals(project, actualProject);
        }
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testUpdateByIdOtherUserIdNegative() throws AbstractException {
        @NotNull final String userId = "OtherUserId";
        @NotNull final String id = projectList.get(0).getId();
        @NotNull final String name = "Project " + id;
        @NotNull final String description = "Desc " + id;
        assertNotNull(projectService.updateProjectById(userId, id, name, description));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIdNullUserIdNegative() throws AbstractException {
        @NotNull final String id = projectList.get(0).getId();
        @NotNull final String name = "Project " + id;
        @NotNull final String description = "Desc " + id;
        assertNotNull(projectService.updateProjectById(null, id, name, description));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIdEmptyUserIdNegative() throws AbstractException {
        @NotNull final String id = projectList.get(0).getId();
        @NotNull final String name = "Project " + id;
        @NotNull final String description = "Desc " + id;
        assertNotNull(projectService.updateProjectById("", id, name, description));
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateByNullIdNegative() throws AbstractException {
        @Nullable final String userId = projectList.get(0).getUserId();
        @NotNull final String name = "Project ";
        @NotNull final String description = "Desc ";
        assertNotNull(projectService.updateProjectById(userId, null, name, description));
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateByEmptyIdNegative() throws AbstractException {
        @NotNull final String id = "";
        @Nullable final String userId = projectList.get(0).getUserId();
        @NotNull final String name = "Project " + id;
        @NotNull final String description = "Desc " + id;
        assertNotNull(projectService.updateProjectById(userId, id, name, description));
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIdNullNameNegative() throws AbstractException {
        @NotNull final String id = projectList.get(0).getId();
        @Nullable final String userId = projectList.get(0).getUserId();
        @NotNull final String description = "Desc " + id;
        assertNotNull(projectService.updateProjectById(userId, id, null, description));
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIdEmptyNameNegative() throws AbstractException {
        @NotNull final String id = projectList.get(0).getId();
        @Nullable final String userId = projectList.get(0).getUserId();
        @NotNull final String name = "";
        @NotNull final String description = "Desc " + id;
        assertNotNull(projectService.updateProjectById(userId, id, name, description));
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testUpdateByIdNullDescNegative() throws AbstractException {
        @NotNull final String id = projectList.get(0).getId();
        @Nullable final String userId = projectList.get(0).getUserId();
        @NotNull final String name = "Project " + id;
        assertNotNull(projectService.updateProjectById(userId, id, name, null));
    }

    @Test
    public void testUpdateByIndexPositive() throws AbstractException {
        for(@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Project> projects = projectList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (int i = 0; i < projects.size(); i++) {
                @NotNull final String name = "Project new " + i;
                @NotNull final String description = "Desc new " + i;
                assertNotNull(projectService.findOneByIndex(userId, i));
                @Nullable final Project actualProject = projectService.updateProjectByIndex(userId, i, name, description);
                assertEquals(name, actualProject.getName());
                assertEquals(description, actualProject.getDescription());
                assertEquals(projects.get(i), actualProject);
            }
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndexNegative() throws AbstractException {
        @NotNull final String userId = "OtherUserId";
        final int index = projectList.size() + 1;
        @NotNull final String name = "Project new " + index;
        @NotNull final String description = "Desc new " + index;
        assertNotNull(projectService.updateProjectByIndex(userId, index, name, description));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByNullIndexNegative() throws AbstractException {
        @NotNull final String userId = userList.get(0).getId();
        final int index = projectList.size() + 1;
        @NotNull final String name = "Project new " + index;
        @NotNull final String description = "Desc new " + index;
        assertNotNull(projectService.updateProjectByIndex(userId, null, name, description));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByOutIndexNegative() throws AbstractException {
        @NotNull final String userId = userList.get(0).getId();
        final int index = projectList.size() + 1;
        @NotNull final String name = "Project new " + index;
        @NotNull final String description = "Desc new " + index;
        assertNotNull(projectService.updateProjectByIndex(userId, index, name, description));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndexOtherUserIdNegative() throws AbstractException {
        @NotNull final String userId = "OtherUserId";
        final int index = 0;
        @NotNull final String name = "Project new " + index;
        @NotNull final String description = "Desc new " + index;
        assertNotNull(projectService.updateProjectByIndex(userId, index, name, description));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIndexNullUserIdNegative() throws AbstractException {
        final int index = 0;
        @NotNull final String name = "Project new " + index;
        @NotNull final String description = "Desc new " + index;
        assertNotNull(projectService.updateProjectByIndex(null, index, name, description));
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIndexNullNameNegative() throws AbstractException {
        @NotNull final String userId = userList.get(0).getId();
        final int index = 0;
        @NotNull final String description = "Desc new " + index;
        assertNotNull(projectService.updateProjectByIndex(userId, index, null, description));
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIndexEmptyNameNegative() throws AbstractException {
        @NotNull final String userId = userList.get(0).getId();
        final int index = 0;
        @NotNull final String name = "";
        @NotNull final String description = "Desc new " + index;
        assertNotNull(projectService.updateProjectByIndex(userId, index, name, description));
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testUpdateByIndexNullDescNegative() throws AbstractException {
        @NotNull final String userId = userList.get(0).getId();
        final int index = 0;
        @NotNull final String name = "Name new " + index;
        assertNotNull(projectService.updateProjectByIndex(userId, index, name, null));
    }

}
