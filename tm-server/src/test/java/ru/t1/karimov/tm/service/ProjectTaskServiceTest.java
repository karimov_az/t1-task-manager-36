package ru.t1.karimov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.api.repository.IUserRepository;
import ru.t1.karimov.tm.api.service.*;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.karimov.tm.exception.entity.TaskNotFoundException;
import ru.t1.karimov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.karimov.tm.exception.field.TaskIdEmptyException;
import ru.t1.karimov.tm.exception.field.UserIdEmptyException;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.repository.ProjectRepository;
import ru.t1.karimov.tm.repository.TaskRepository;
import ru.t1.karimov.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;


@Category(UnitCategory.class)
public class ProjectTaskServiceTest {

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IProjectTaskService projectTaskService;

    @NotNull
    private List<Task> taskList;

    @NotNull
    private List<User> userList;

    @Before
    public void initTest() throws AbstractException {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IUserService userService;
        projectService = new ProjectService(projectRepository);
        taskService = new TaskService(taskRepository);
        projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
        userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);
        projectList = new ArrayList<>();
        taskList = new ArrayList<>();
        userList = new ArrayList<>();

        @NotNull final User userAdmin = new User();
        userAdmin.setLogin("admin");
        userAdmin.setPasswordHash("admin");
        userAdmin.setEmail("admin@tst.ru");
        userAdmin.setRole(Role.ADMIN);
        @NotNull final User userTest = new User();
        userTest.setLogin("test");
        userTest.setPasswordHash("test");
        userTest.setEmail("test@tst.ru");
        userTest.setRole(Role.USUAL);
        @NotNull final String adminId = userAdmin.getId();
        @NotNull final String userId = userTest.getId();

        userService.add(userAdmin);
        userService.add(userTest);
        userList.add(userAdmin);
        userList.add(userTest);
        projectList.add(new Project(adminId, "Project 1", "Desc 1", Status.IN_PROGRESS));
        projectList.add(new Project(adminId, "Project 2", "Desc 2", Status.NOT_STARTED));
        projectList.add(new Project(userId, "Project 1", "Desc 1", Status.IN_PROGRESS));
        projectList.add(new Project(userId, "Project 2", "Desc 2", Status.IN_PROGRESS));
        projectList.add(new Project(userId, "Project 3", "Desc 3", Status.NOT_STARTED));
        taskList.add(new Task(adminId, "Task 1", "Desc 1", Status.NOT_STARTED, projectList.get(0).getId()));
        taskList.add(new Task(adminId, "Task 2", "Desc 2", Status.IN_PROGRESS, null));
        taskList.add(new Task(adminId, "Task 3", "Desc 3", Status.IN_PROGRESS, null));
        taskList.add(new Task(userId, "Task 1", "Desc 1", Status.IN_PROGRESS, projectList.get(4).getId()));
        taskList.add(new Task(userId, "Task 2", "Desc 2", Status.IN_PROGRESS, projectList.get(4).getId()));
        taskList.add(new Task(userId, "Task 3", "Desc 3", Status.IN_PROGRESS, null));
        taskList.add(new Task(userId, "Task 3", "Desc 3", Status.IN_PROGRESS, null));
        for (@NotNull final Project project : projectList) projectService.add(project);
        for (@NotNull final Task task : taskList) taskService.add(task);
    }

    @Test
    public void testBindTaskToProject() throws AbstractException {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final Project userProject = projectService.findAll(userId).get(0);
            @NotNull final String projectId = userProject.getId();
            @NotNull final List<Task> expectedTasks = taskList.stream()
                    .filter(m -> m.getProjectId() == null)
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (@NotNull final Task task : expectedTasks) {
                @NotNull final String taskId = task.getId();
                projectTaskService.bindTaskToProject(userId, projectId, taskId);
                @Nullable final Task actualTask = taskService.findOneById(userId, taskId);
                assertNotNull(actualTask);
                assertEquals(projectId, actualTask.getProjectId());

                assertThrows(
                        ProjectNotFoundException.class,
                        () -> projectTaskService.bindTaskToProject(userId, taskId, projectId)
                );
                assertThrows(
                        UserIdEmptyException.class,
                        () -> projectTaskService.bindTaskToProject("", projectId, taskId)
                );
                assertThrows(
                        TaskNotFoundException.class,
                        () -> projectTaskService.bindTaskToProject(userId, projectId, projectId)
                );
                assertThrows(
                        UserIdEmptyException.class,
                        () -> projectTaskService.bindTaskToProject(null, projectId, taskId)
                );
                assertThrows(
                        ProjectIdEmptyException.class,
                        () -> projectTaskService.bindTaskToProject(userId, "", taskId)
                );
                assertThrows(
                        ProjectIdEmptyException.class,
                        () -> projectTaskService.bindTaskToProject(userId, null, taskId)
                );
                assertThrows(
                        TaskIdEmptyException.class,
                        () -> projectTaskService.bindTaskToProject(userId, projectId, "")
                );
                assertThrows(
                        TaskIdEmptyException.class,
                        () -> projectTaskService.bindTaskToProject(userId, projectId, null)
                );
            }
        }
    }

    @Test
    public void testRemoveProjectById() throws AbstractException {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final Task expectedTask = taskList.stream()
                    .filter(m -> m.getProjectId() != null)
                    .filter(m -> userId.equals(m.getUserId()))
                    .findFirst()
                    .orElse(new Task(userId,"Task 10", "Desc 10", Status.NOT_STARTED, projectList.get(0).getId()));
            @Nullable final String projectId = expectedTask.getProjectId();
            projectTaskService.removeProjectById(userId, projectId);
            @NotNull final List<Task> actualList = taskService.findAllByProjectId(userId, projectId);
            assertEquals(0, actualList.size());

            assertThrows(
                    UserIdEmptyException.class,
                    () -> projectTaskService.removeProjectById(null, projectId)
            );
            assertThrows(
                    UserIdEmptyException.class,
                    () -> projectTaskService.removeProjectById("", projectId)
            );
            assertThrows(
                    ProjectIdEmptyException.class,
                    () -> projectTaskService.removeProjectById(userId, null)
            );
            assertThrows(
                    ProjectIdEmptyException.class,
                    () -> projectTaskService.removeProjectById(userId, "")
            );
            assertThrows(
                    ProjectNotFoundException.class,
                    () -> projectTaskService.removeProjectById("otherId", projectId)
            );
            assertThrows(
                    ProjectNotFoundException.class,
                    () -> projectTaskService.removeProjectById(userId, "otherId")
            );
        }
    }

    @Test
    public void testUnbindTaskToProject() throws AbstractException {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Task> expectedTasks = taskList.stream()
                    .filter(m -> m.getProjectId() != null)
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (@NotNull final Task task : expectedTasks) {
                @NotNull final String taskId = task.getId();
                @Nullable final String projectId = task.getProjectId();
                projectTaskService.unbindTaskFromProject(userId, projectId, taskId);
                @Nullable final Task actualTask = taskService.findOneById(userId, taskId);
                assertNotNull(actualTask);
                assertNull(actualTask.getProjectId());

                assertThrows(
                        ProjectNotFoundException.class,
                        () -> projectTaskService.unbindTaskFromProject(userId, taskId, projectId)
                );
                assertThrows(
                        UserIdEmptyException.class,
                        () -> projectTaskService.unbindTaskFromProject("", projectId, taskId)
                );
                assertThrows(
                        TaskNotFoundException.class,
                        () -> projectTaskService.unbindTaskFromProject(userId, projectId, projectId)
                );
                assertThrows(
                        UserIdEmptyException.class,
                        () -> projectTaskService.unbindTaskFromProject(null, projectId, taskId)
                );
                assertThrows(
                        ProjectIdEmptyException.class,
                        () -> projectTaskService.unbindTaskFromProject(userId, "", taskId)
                );
                assertThrows(
                        ProjectIdEmptyException.class,
                        () -> projectTaskService.unbindTaskFromProject(userId, null, taskId)
                );
                assertThrows(
                        TaskIdEmptyException.class,
                        () -> projectTaskService.unbindTaskFromProject(userId, projectId, "")
                );
                assertThrows(
                        TaskIdEmptyException.class,
                        () -> projectTaskService.unbindTaskFromProject(userId, projectId, null)
                );
            }
        }
    }

}
