package ru.t1.karimov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.enumerated.TaskSort;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.field.UserIdEmptyException;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.model.Task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@Category(UnitCategory.class)
public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final int HALF_NUMBER_OF_ENTRIES = NUMBER_OF_ENTRIES / 2;

    private static final int DOUBLE_NUMBER_OF_ENTRIES = NUMBER_OF_ENTRIES * 2;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private static final String PROJECT_ID_1 = UUID.randomUUID().toString();

    private static final String PROJECT_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private List<Task> taskList;

    @NotNull
    private ITaskRepository taskRepository;

    @Before
    public void initRepository() throws AbstractException {
        taskList = new ArrayList<>();
        taskRepository = new TaskRepository();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task " + i);
            task.setDescription("Description " + i);
            if (i == 2 || i == 7 ) task.setStatus(Status.IN_PROGRESS);
            if (i < HALF_NUMBER_OF_ENTRIES) task.setUserId(USER_ID_1);
            else task.setUserId(USER_ID_2);
            if (i == 1 || i == 2 ) task.setProjectId(PROJECT_ID_1);
            if (i == 5 || i == 7 ) task.setProjectId(PROJECT_ID_2);
            taskRepository.add(task);
            taskList.add(task);
        }
    }

    @Test
    public void testAddTask() throws AbstractException {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Task newTask = new Task();
        taskRepository.add(newTask);
        assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testAdd() throws AbstractException {
        @NotNull final Task task = new Task();
        @NotNull final String userId = USER_ID_1;
        @NotNull final String name = "Test Project Name";
        @NotNull final String description = "Test Project Description";
        @NotNull final String id = task.getId();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        taskRepository.add(USER_ID_1, task);
        assertEquals(NUMBER_OF_ENTRIES + 1, taskRepository.getSize());
        @Nullable final Task actualTask = taskRepository.findOneById(id);
        assertNotNull(actualTask);
        assertEquals(userId, actualTask.getUserId());
        assertEquals(name, actualTask.getName());
        assertEquals(description, actualTask.getDescription());
    }

    @Test
    public void testAddAll() {
        @NotNull List<Task> taskList = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final String userId = i < HALF_NUMBER_OF_ENTRIES ? USER_ID_2 : USER_ID_1;
            final int num = i + 1 + NUMBER_OF_ENTRIES;
            @NotNull final String name = "Test Project Name " + num;
            @NotNull final String description = "Test Project Description " + num;
            @NotNull final Task task = new Task(userId, name, description, Status.IN_PROGRESS, "");
            taskList.add(task);
        }
        taskRepository.add(taskList);
        assertEquals(DOUBLE_NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testClear() {
        taskRepository.removeAll();
        assertEquals(0, taskRepository.getSize());
    }

    @Test
    public void testClearForUserPositive() {
        @NotNull final List<Task> emptyList = new ArrayList<>();
        taskRepository.removeAll(USER_ID_1);
        assertEquals(emptyList, taskRepository.findAll(USER_ID_1));
        assertNotEquals(emptyList, taskRepository.findAll(USER_ID_2));
    }

    @Test
    public void testClearForUserNegative() {
        taskRepository.removeAll("Other_id");
        assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Task> actualProjectList = taskRepository.findAll();
        assertEquals(taskList, actualProjectList);
    }

    @Test
    public void testFindAllByUser() {
        assertEquals(taskList.subList(0, 5), taskRepository.findAll(USER_ID_1));
        assertEquals(taskList.subList(5, NUMBER_OF_ENTRIES), taskRepository.findAll(USER_ID_2));
    }

    @Test
    public void testFindAllSort() {
        for (@NotNull final TaskSort sort : TaskSort.values()) {
            @NotNull final List<Task> actualTaskList = taskRepository.findAll(sort.getComparator());
            taskList.sort(sort.getComparator());
            assertEquals(taskList, actualTaskList);
        }
    }

    @Test
    public void testFindAllSortByUser() {
        for (@NotNull final TaskSort sort : TaskSort.values()) {
            @NotNull final List<Task> actualTaskList = taskRepository.findAll(USER_ID_1, sort.getComparator());
            @NotNull final List<Task> tasks = taskList.subList(0, 5);
            tasks.sort(sort.getComparator());
            assertEquals(tasks, actualTaskList);
        }
        for (@NotNull final TaskSort sort : TaskSort.values()) {
            @NotNull final List<Task> actualTaskList = taskRepository.findAll(USER_ID_2, sort.getComparator());
            @NotNull final List<Task> tasks = taskList.subList(5, NUMBER_OF_ENTRIES);
            tasks.sort(sort.getComparator());
            assertEquals(tasks, actualTaskList);
        }
    }

    @Test
    public void testFindOneByIdPositive() throws AbstractException {
        for (@NotNull final Task task : taskList) {
            @NotNull final String id = task.getId();
            assertEquals(task, taskRepository.findOneById(id));
        }
    }

    @Test
    public void testFindOneByIdNegative() throws AbstractException {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final String id = UUID.randomUUID().toString();
            assertNull(taskRepository.findOneById(id));
        }
    }

    @Test
    public void testFindOneByIdForUserPositive() throws AbstractException {
        for (@NotNull final Task task : taskList) {
            @Nullable final String userId = task.getUserId();
            if (userId == null) throw new UserIdEmptyException();
            @NotNull final String id = task.getId();
            assertNotNull(taskRepository.findOneById(userId, id));
            assertEquals(task, taskRepository.findOneById(userId, id));
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            assertNull(taskRepository.findOneById(USER_ID_1, UUID.randomUUID().toString()));
        }
    }

    @Test
    public void testFindOneByIndexPositive() throws AbstractException {
        int i = 0;
        for (@NotNull final Task task : taskList) {
            @Nullable final Task actualTask = taskRepository.findOneByIndex(i);
            assertNotNull(actualTask);
            assertEquals(task, actualTask);
            i++;
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testFindOneByIndexNegative() throws AbstractException {
        assertNull(taskRepository.findOneByIndex(NUMBER_OF_ENTRIES + 1));
    }

    @Test
    public void testFindOneByIndexForUserPositive() {
        int i = 0, j = 0;
        for (@NotNull final Task task : taskList) {
            @Nullable final String userId = task.getUserId();
            if (USER_ID_1.equals(userId)) {
                @Nullable final Task actualTask = taskRepository.findOneByIndex(USER_ID_1, i);
                assertNotNull(actualTask);
                assertEquals(task, actualTask);
                i++;
            }
            if (USER_ID_2.equals(userId)) {
                @Nullable final Task actualTask = taskRepository.findOneByIndex(USER_ID_2, j);
                assertNotNull(actualTask);
                assertEquals(task, actualTask);
                j++;
            }
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testFindOneByIndexForUserNegative() {
        @NotNull final String userId = UUID.randomUUID().toString();
        assertNull(taskRepository.findOneByIndex(userId, NUMBER_OF_ENTRIES + 1));
    }

    @Test
    public void testFindAllByProjectIdForUserPositive() {
        @NotNull final List<String> userList = Arrays.asList(USER_ID_1, USER_ID_2);
        for (@NotNull final String userId : userList) {
            @NotNull final String projectId = userId.equals(USER_ID_1) ? PROJECT_ID_1 : PROJECT_ID_2;
            @NotNull final List<Task> expectedList = taskList.stream()
                    .filter(m -> m.getProjectId() != null)
                    .filter(m -> userId.equals(m.getUserId()))
                    .filter(m -> projectId.equals(m.getProjectId()))
                    .collect(Collectors.toList());
            @NotNull final List<Task> actualList = taskRepository.findAllByProjectId(userId, projectId);
            assertEquals(expectedList, actualList);
            assertEquals(2, actualList.size());
        }
    }

    @Test
    public void testFindAllByProjectIdForUserNegative() {
        @NotNull final List<String> userList = Arrays.asList(USER_ID_1, USER_ID_2);
        for (@NotNull final String userId : userList) {
            @NotNull final String projectId = userId.equals(USER_ID_1) ? PROJECT_ID_2 : PROJECT_ID_1;
            @NotNull final List<Task> expectedList = taskList.stream()
                    .filter(m -> m.getProjectId() != null)
                    .filter(m -> userId.equals(m.getUserId()))
                    .filter(m -> projectId.equals(m.getProjectId()))
                    .collect(Collectors.toList());
            @NotNull final List<Task> actualList = taskRepository.findAllByProjectId(userId, projectId);
            assertEquals(expectedList, actualList);
            assertEquals(0, actualList.size());
        }
    }

    @Test
    public void testGetSize() {
        final int taskRepositorySize = taskRepository.getSize();
        assertEquals(taskList.size(), taskRepositorySize);
        assertEquals(NUMBER_OF_ENTRIES, taskRepositorySize);
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final List<String> userList = Arrays.asList(USER_ID_1, USER_ID_2);
        final int expectedSize = NUMBER_OF_ENTRIES / userList.size();
        for (@NotNull final String userId : userList) {
            final int taskRepositorySize = taskRepository.getSize(userId);
            @Nullable final List<Task> projectsUser = taskList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            assertEquals(projectsUser.size(), taskRepositorySize);
            assertEquals(expectedSize, taskRepositorySize);
        }
    }

    @Test
    public void testRemovePositive() throws AbstractException {
        for (@NotNull final Task task : taskList) {
            taskRepository.removeOne(task);
            assertNull(taskRepository.findOneById(task.getId()));
        }
        assertNotEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testRemoveNegative() throws AbstractException {
        @NotNull final Task task = new Task();
        taskRepository.removeOne(task);
        assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testRemoveByIdPositive() throws AbstractException {
        for (@NotNull final Task task : taskList) {
            assertNotNull(taskRepository.removeOneById(task.getId()));
            assertNull(taskRepository.findOneById(task.getId()));
        }
    }

    @Test
    public void testRemoveByIdNegative() throws AbstractException {
        @NotNull final String idNotFromRepository = UUID.randomUUID().toString();
        assertNull(taskRepository.removeOneById(idNotFromRepository));
    }

    @Test
    public void testRemoveByIdForUserPositive() {
        for (@NotNull final Task task : taskList) {
            @Nullable final String userId = task.getUserId();
            @NotNull final String id = task.getId();
            if (USER_ID_1.equals(userId)) {
                assertNotNull(taskRepository.removeOneById(USER_ID_1, id));
                assertNull(taskRepository.findOneById(USER_ID_1, id));
            } else {
                assertNull(taskRepository.removeOneById(USER_ID_1, id));
            }
        }
    }

    @Test
    public void testRemoveByIdForUserNegative() {
        @NotNull final String otherUserId = UUID.randomUUID().toString();
        @NotNull final String otherId = UUID.randomUUID().toString();
        assertNull(taskRepository.removeOneById(USER_ID_1, otherId));
        assertNull(taskRepository.removeOneById(USER_ID_2, otherId));
        assertNull(taskRepository.removeOneById(otherUserId,otherId));
    }

    @Test
    public void testRemoveByIndexPositive() throws AbstractException {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @Nullable final Task task = taskList.get(i);
            @Nullable final Task actualTask = taskRepository.removeOneByIndex(0);
            assertNotNull(actualTask);
            assertNotNull(task);
            assertEquals(task, actualTask);
        }
        assertEquals(0, taskRepository.getSize());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveByIndexNegative() throws AbstractException {
        final int index = NUMBER_OF_ENTRIES + 1;
        @Nullable final Task actualTask = taskRepository.removeOneByIndex(index);
        assertNull(actualTask);
    }

    @Test
    public void testRemoveByIndexForUserPositive() {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = taskList.get(i);
            @Nullable final String userId = task.getUserId();
            if (USER_ID_1.equals(userId)) {
                @Nullable final Task actualTask = taskRepository.removeOneByIndex(USER_ID_1, 0);
                assertNotNull(actualTask);
                assertEquals(task, actualTask);
            }
            if (USER_ID_2.equals(userId)) {
                @Nullable final Task actualTask = taskRepository.removeOneByIndex(USER_ID_2, 0);
                assertNotNull(actualTask);
                assertEquals(task, actualTask);
            }
        }
        assertEquals(0, taskRepository.getSize());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveByIndexForUserNegative() {
        final int index = NUMBER_OF_ENTRIES + 1;
        @NotNull final String otherUserId = UUID.randomUUID().toString();
        assertNull(taskRepository.removeOneByIndex(otherUserId, index));
    }

}
