package ru.t1.karimov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.api.repository.IUserRepository;
import ru.t1.karimov.tm.api.service.IProjectService;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.api.service.ITaskService;
import ru.t1.karimov.tm.api.service.IUserService;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.entity.UserNotFoundException;
import ru.t1.karimov.tm.exception.field.EmailEmptyException;
import ru.t1.karimov.tm.exception.field.IdEmptyException;
import ru.t1.karimov.tm.exception.field.LoginEmptyException;
import ru.t1.karimov.tm.exception.field.PasswordEmptyException;
import ru.t1.karimov.tm.exception.user.ExistsEmailException;
import ru.t1.karimov.tm.exception.user.ExistsLoginException;
import ru.t1.karimov.tm.exception.user.RoleEmptyException;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.repository.ProjectRepository;
import ru.t1.karimov.tm.repository.TaskRepository;
import ru.t1.karimov.tm.repository.UserRepository;
import ru.t1.karimov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(UnitCategory.class)
public class UserServiceTest {

    @NotNull
    private IUserService userService;

    @NotNull
    private List<User> userList;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @Before
    public void initTest() throws AbstractException {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);
        projectService = new ProjectService(projectRepository);
        taskService = new TaskService(taskRepository);

        @NotNull final User userAdmin = new User();
        userAdmin.setLogin("admin");
        userAdmin.setPasswordHash(HashUtil.salt(propertyService, "admin"));
        userAdmin.setEmail("admin@tst.ru");
        userAdmin.setRole(Role.ADMIN);
        @NotNull final User userTest = new User();
        userTest.setLogin("test");
        userTest.setPasswordHash(HashUtil.salt(propertyService, "test"));
        userTest.setEmail("test@tst.ru");
        userTest.setRole(Role.USUAL);
        @NotNull final User userTom = new User();
        userTom.setLogin("tom");
        userTom.setPasswordHash(HashUtil.salt(propertyService, "tom"));
        userTom.setEmail("tom@tst.ru");
        userTom.setRole(Role.USUAL);
        @NotNull final User userSoul = new User();
        userSoul.setLogin("soul");
        userSoul.setPasswordHash(HashUtil.salt(propertyService, "soul"));
        userSoul.setEmail("soul@tst.ru");
        userSoul.setRole(Role.USUAL);
        userSoul.setLocked(true);

        userList = new ArrayList<>();
        userList.add(userAdmin);
        userList.add(userTest);
        userList.add(userTom);
        userList.add(userSoul);
        userService.add(userAdmin);
        userService.add(userTest);
        userService.add(userTom);
        userService.add(userSoul);
    }

    @Test
    public void testAdd() throws AbstractException {
        @NotNull final User user = new User();
        user.setLogin("user");
        user.setPasswordHash("user");
        user.setEmail("user@tst.ru");
        user.setRole(Role.USUAL);
        userService.add(user);
        assertEquals(userList.size() + 1, userService.getSize());
    }

    @Test
    public void testAddAll() {
        @NotNull List<User> users = new ArrayList<>();
        final int expectedCount = userList.size();
        for (int i = 0; i < expectedCount; i++) {
            users.add(new User());
        }
        userService.add(users);
        assertEquals(expectedCount * 2, userService.getSize());
    }

    @Test
    public void testCreate() throws AbstractException {
        @NotNull final String login = "user";
        @NotNull final String password = "user";
        @NotNull final String email = "user@tst.ru";
        @NotNull final Role role = Role.USUAL;
        @NotNull final String loginUnix = "unix";
        @NotNull final String passwordUnix = "unix";
        @NotNull final String loginLinux = "linux";
        @NotNull final String passwordLinux = "linux";

        assertNotNull(userService.create(login, password));
        assertNotNull(userService.create(loginUnix, passwordUnix, email));
        assertNotNull(userService.create(loginLinux, passwordLinux, role));
        assertEquals(userList.size() + 3, userService.getSize());
    }

    @Test
    public void testCreateException() {
        @NotNull final String login = "user";
        @NotNull final String password = "user";
        @NotNull final String email = "user@tst.ru";
        @NotNull final Role role = Role.USUAL;
        @NotNull final String loginTest = "test";
        @NotNull final String passwordTest = "test";
        @NotNull final String emailTest = "test@tst.ru";
        @Nullable final Role roleNull = null;

        assertThrows(
                LoginEmptyException.class,
                () -> userService.create(null, password)
        );
        assertThrows(
                LoginEmptyException.class,
                () -> userService.create("", password)
        );
        assertThrows(
                ExistsLoginException.class,
                () -> userService.create(loginTest, passwordTest)
        );
        assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(login, null)
        );
        assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(login, "")
        );
        assertThrows(
                LoginEmptyException.class,
                () -> userService.create(null, password, email)
        );
        assertThrows(
                LoginEmptyException.class,
                () -> userService.create("", password, email)
        );
        assertThrows(
                ExistsLoginException.class,
                () -> userService.create(loginTest, passwordTest, email)
        );
        assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(login, null, email)
        );
        assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(login, "", email)
        );
        assertThrows(
                ExistsEmailException.class,
                () -> userService.create(login, password, emailTest)
        );
        assertThrows(
                LoginEmptyException.class,
                () -> userService.create(null, password, role)
        );
        assertThrows(
                LoginEmptyException.class,
                () -> userService.create("", password, role)
        );
        assertThrows(
                ExistsLoginException.class,
                () -> userService.create(loginTest, passwordTest, role)
        );
        assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(login, null, role)
        );
        assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(login, "", role)
        );
        assertThrows(
                RoleEmptyException.class,
                () -> userService.create(login, password, roleNull)
        );
    }

    @Test
    public void testClear() {
        userService.removeAll();
        assertEquals(0, userService.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull List<User> userList = userService.findAll();
        assertEquals(4, userList.size());
    }

    @Test
    public void testFindById() throws AbstractException {
        for (@NotNull final User user : userList) {
            @NotNull final String id = user.getId();
            @Nullable final User actualUser = userService.findOneById(id);
            assertNotNull(actualUser);
            assertEquals(user, actualUser);
        }
        assertThrows(
                IdEmptyException.class, () -> userService.findOneById(null)
        );
        assertThrows(
                IdEmptyException.class, () -> userService.findOneById("")
        );
        assertNull(userService.findOneById("otherId"));
    }

    @Test
    public void testFindByLogin() throws AbstractException {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            @Nullable final User actualUser =userService.findByLogin(login);
            assertNotNull(actualUser);
            assertEquals(user, actualUser);
        }
        assertThrows(
                LoginEmptyException.class, () -> userService.findByLogin(null)
        );
        assertThrows(
                LoginEmptyException.class, () -> userService.findByLogin("")
        );
        assertNull(userService.findByLogin("otherLogin"));
    }

    @Test
    public void testFindByEmail() throws AbstractException {
        for (@NotNull final User user : userList) {
            @Nullable final String email = user.getEmail();
            @Nullable final User actualUser =userService.findByEmail(email);
            assertNotNull(actualUser);
            assertEquals(user, actualUser);
        }
        assertThrows(
                EmailEmptyException.class, () -> userService.findByEmail(null)
        );
        assertThrows(
                EmailEmptyException.class, () -> userService.findByEmail("")
        );
        assertNull(userService.findByEmail("otherEmail"));
    }

    @Test
    public void testGetSize() {
        assertEquals(userList.size(), userService.getSize());
    }

    @Test
    public void testIsEmailExist() {
        for (@NotNull final User user : userList) {
            @Nullable final String email = user.getEmail();
            assertTrue(userService.isEmailExist(email));
        }
        assertFalse(userService.isEmailExist("otherEmail"));
        assertFalse(userService.isEmailExist(""));
        assertFalse(userService.isEmailExist(null));
    }

    @Test
    public void testIsLoginExist() {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            assertTrue(userService.isLoginExist(login));
        }
        assertFalse(userService.isLoginExist("otherLogin"));
        assertFalse(userService.isLoginExist(""));
        assertFalse(userService.isLoginExist(null));
    }

    @Test
    public void testLockUserByLogin() throws AbstractException {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            userService.lockUserByLogin(login);
            @Nullable final User actualUser = userService.findByLogin(login);
            assertNotNull(actualUser);
            assertTrue(actualUser.getLocked());
        }
        assertThrows(
                LoginEmptyException.class, () -> userService.lockUserByLogin(null)
        );
        assertThrows(
                LoginEmptyException.class, () -> userService.lockUserByLogin("")
        );
        assertThrows(
                UserNotFoundException.class, () -> userService.lockUserByLogin("otherLogin")
        );
    }

    @Test
    public void testRemoveOne() throws AbstractException {
        @Nullable final User userAdmin = userService.findByLogin("admin");
        assertNotNull(userAdmin);
        @NotNull final String adminId = userAdmin.getId();
        @NotNull final Status status = Status.NOT_STARTED;
        @NotNull final Project projectAdmin = projectService.add(new Project(adminId, "P1", "PD1", status));
        @NotNull final String adminProjectId = projectAdmin.getId();
        taskService.add(new Task(adminId, "T1", "TD1", status, adminProjectId));
        assertTrue(projectService.getSize(adminId) > 0);
        assertTrue(taskService.getSize(adminId) > 0);

        for (@NotNull final User user : userList) assertNotNull(userService.removeOne(user));
        assertEquals(0, userService.getSize());
        assertEquals(0, projectService.getSize());
        assertEquals(0, taskService.getSize());

        assertNull(userService.removeOne(null));
    }

    @Test
    public void testRemoveByLogin() throws AbstractException {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            assertNotNull(userService.removeOneByLogin(login));
        }
        assertEquals(0, userService.getSize());

        assertThrows(
                LoginEmptyException.class, () -> userService.removeOneByLogin(null)
        );
        assertThrows(
                LoginEmptyException.class, () -> userService.removeOneByLogin("")
        );
        assertNull(userService.removeOneByLogin("otherLogin"));
    }

    @Test
    public void testRemoveByEmail() throws AbstractException {
        for (@NotNull final User user : userList) {
            @Nullable final String email = user.getEmail();
            assertNotNull(userService.removeOneByEmail(email));
        }
        assertEquals(0, userService.getSize());

        assertThrows(
                EmailEmptyException.class, () -> userService.removeOneByEmail(null)
        );
        assertThrows(
                EmailEmptyException.class, () -> userService.removeOneByEmail("")
        );
        assertNull(userService.removeOneByEmail("otherEmail"));
    }

    @Test
    public void testSetPassword() throws AbstractException {
        for (@NotNull final User user : userList) {
            @NotNull final IPropertyService propertyService = new PropertyService();
            @NotNull final String id = user.getId();
            @Nullable final String password = user.getPasswordHash();
            @NotNull final User actualUser = userService.setPassword(id, "newPassword");
            assertNotEquals(password, actualUser.getPasswordHash());
            assertEquals(HashUtil.salt(propertyService, "newPassword"), actualUser.getPasswordHash());

            assertThrows(
                    PasswordEmptyException.class, () -> userService.setPassword(id, "")
            );
            assertThrows(
                    PasswordEmptyException.class, () -> userService.setPassword(id, null)
            );
        }

        assertThrows(
                IdEmptyException.class, () -> userService.setPassword(null, "newPassword")
        );
        assertThrows(
                IdEmptyException.class, () -> userService.setPassword("", "newPassword")
        );
        assertThrows(
                UserNotFoundException.class, () -> userService.setPassword("otherId", "newPassword")
        );
    }

    @Test
    public void testUpdateUser() throws AbstractException {
        @NotNull final String firstName = "New First Name";
        @NotNull final String lastName = "New Last Name";
        @NotNull final String middleName = "New Middle Name";
        for (@NotNull final User user : userList) {
            @NotNull final String id = user.getId();
            @NotNull final User actualUser = userService.updateUser(id,firstName, lastName, middleName);
            assertEquals(firstName, actualUser.getFirstName());
            assertEquals(lastName, actualUser.getLastName());
            assertEquals(middleName, actualUser.getMiddleName());
        }
        assertEquals(userList.size(), userService.getSize());

        assertThrows(
                IdEmptyException.class, () -> userService.updateUser("",firstName, lastName, middleName)
        );
        assertThrows(
                IdEmptyException.class, () -> userService.updateUser(null,firstName, lastName, middleName)
        );
        assertThrows(
                UserNotFoundException.class, () -> userService.updateUser("otherId",firstName, lastName, middleName)
        );
    }

    @Test
    public void testUnlockUserByLogin() throws AbstractException {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            userService.unlockUserByLogin(login);
            @Nullable final User actualUser = userService.findByLogin(login);
            assertNotNull(actualUser);
            assertFalse(actualUser.getLocked());
        }
        assertThrows(
                LoginEmptyException.class, () -> userService.unlockUserByLogin(null)
        );
        assertThrows(
                LoginEmptyException.class, () -> userService.unlockUserByLogin("")
        );
        assertThrows(
                UserNotFoundException.class, () -> userService.unlockUserByLogin("otherLogin")
        );
    }

}
