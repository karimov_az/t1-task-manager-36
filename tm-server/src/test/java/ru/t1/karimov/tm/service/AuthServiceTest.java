package ru.t1.karimov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.api.repository.ISessionRepository;
import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.api.repository.IUserRepository;
import ru.t1.karimov.tm.api.service.IAuthService;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.api.service.ISessionService;
import ru.t1.karimov.tm.api.service.IUserService;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.field.LoginEmptyException;
import ru.t1.karimov.tm.exception.field.PasswordEmptyException;
import ru.t1.karimov.tm.exception.user.AccessDeniedException;
import ru.t1.karimov.tm.exception.user.AuthenticationException;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.model.Session;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.repository.ProjectRepository;
import ru.t1.karimov.tm.repository.SessionRepository;
import ru.t1.karimov.tm.repository.TaskRepository;
import ru.t1.karimov.tm.repository.UserRepository;
import ru.t1.karimov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(UnitCategory.class)
public class AuthServiceTest {

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private IUserService userService;

    @NotNull
    private IAuthService authService;

    @NotNull
    private final List<Session> sessionList = new ArrayList<>();

    @NotNull
    private final List<User> userList = new ArrayList<>();

    @Before
    public void initTest() {
        @NotNull final ISessionRepository sessionRepository = new SessionRepository();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);
        sessionService = new SessionService(sessionRepository);
        authService = new AuthService(propertyService, userService, sessionService);

        @NotNull final User userAdmin = new User();
        userAdmin.setLogin("admin");
        userAdmin.setPasswordHash(HashUtil.salt(propertyService, "admin"));
        userAdmin.setEmail("admin@tst.ru");
        userAdmin.setRole(Role.ADMIN);
        @NotNull final User userTest = new User();
        userTest.setLogin("test");
        userTest.setPasswordHash(HashUtil.salt(propertyService, "test"));
        userTest.setEmail("test@tst.ru");
        userTest.setRole(Role.USUAL);
        @NotNull final User userTom = new User();
        userTom.setLogin("tom");
        userTom.setPasswordHash(HashUtil.salt(propertyService, "tom"));
        userTom.setEmail("tom@tst.ru");
        userTom.setRole(Role.USUAL);
        @NotNull final User userSoul = new User();
        userSoul.setLogin("soul");
        userSoul.setPasswordHash(HashUtil.salt(propertyService, "soul"));
        userSoul.setEmail("soul@tst.ru");
        userSoul.setRole(Role.USUAL);
        userSoul.setLocked(true);
        userList.add(userAdmin);
        userList.add(userTest);
        userList.add(userTom);
        userList.add(userSoul);
        userService.add(userList);

        @NotNull final Session sessionAdmin = new Session();
        @NotNull final Session sessionTest = new Session();
        @Nullable final String adminUserId = userAdmin.getId();
        @Nullable final String testUserId = userTest.getId();
        sessionAdmin.setUserId(adminUserId);
        sessionTest.setUserId(testUserId);
        sessionList.add(sessionAdmin);
        sessionList.add(sessionTest);
        sessionService.add(sessionList);
    }

    @After
    public void initClear() {
        sessionService.removeAll();
        userService.removeAll();
        sessionList.clear();
    }

    @Test
    public void testCheck() throws AbstractException {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            assertNotNull(login);
            @Nullable final String password = login;
            assertNotNull(password);
            if (user.getLocked()) {
                assertThrows(
                        AuthenticationException.class, () -> authService.check(login, password)
                );
                continue;
            }
            @NotNull final User actualUser = authService.check(login, password);
            assertEquals(user, actualUser);

            assertThrows(
                    LoginEmptyException.class, () -> authService.check(null, password)
            );
            assertThrows(
                    LoginEmptyException.class, () -> authService.check("", password)
            );
            assertThrows(
                    PasswordEmptyException.class, () -> authService.check(login, null)
            );
            assertThrows(
                    PasswordEmptyException.class, () -> authService.check(login, "")
            );
            assertThrows(
                    AuthenticationException.class, () -> authService.check("otherLogin", password)
            );
            assertThrows(
                    AuthenticationException.class, () -> authService.check(login, "otherPass")
            );
        }
        assertThrows(
                AuthenticationException.class, () -> authService.check("otherLogin", "otherPass")
        );
    }

    @Test
    public void testInvalidate() throws AbstractException {
        for (@NotNull final Session session : sessionList) {
            authService.invalidate(session);
        }
        assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testLogin() throws AbstractException {
        sessionService.removeAll();
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            assertNotNull(login);
            @Nullable final String password = login;
            assertNotNull(password);
            if (user.getLocked()) {
                assertThrows(
                        AuthenticationException.class, () -> authService.login(login, password)
                );
                continue;
            }
            @NotNull final String actualToken = authService.login(login, password);
            assertNotNull(actualToken);
            assertTrue(actualToken.length() > 0);

            assertThrows(
                    LoginEmptyException.class, () -> authService.login(null, password)
            );
            assertThrows(
                    LoginEmptyException.class, () -> authService.login("", password)
            );
            assertThrows(
                    PasswordEmptyException.class, () -> authService.login(login, null)
            );
            assertThrows(
                    PasswordEmptyException.class, () -> authService.login(login, "")
            );
            assertThrows(
                    AuthenticationException.class, () -> authService.login("otherLogin", password)
            );
            assertThrows(
                    AuthenticationException.class, () -> authService.login(login, "otherPass")
            );
        }
        assertThrows(
                AuthenticationException.class, () -> authService.login("otherLogin", "otherPass")
        );
    }

    @Test
    public void testValidateToken() throws AbstractException {
        sessionService.removeAll();
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            assertNotNull(login);
            @Nullable final String password = login;
            assertNotNull(password);
            if (user.getLocked()) {
                assertThrows(
                        AuthenticationException.class, () -> authService.login(login, password)
                );
                continue;
            }
            @NotNull final String actualToken = authService.login(login, password);
            assertNotNull(actualToken);
            assertTrue(actualToken.length() > 0);
            @NotNull final Session actualSession = authService.validateToken(actualToken);
            assertTrue(sessionService.existsById(actualSession.getId()));
            assertEquals(user.getId(), actualSession.getUserId());

            assertThrows(
                    AccessDeniedException.class, () -> authService.validateToken(null)
            );
            assertThrows(
                    AccessDeniedException.class, () -> authService.validateToken("")
            );
        }
        assertThrows(
                AccessDeniedException.class, () -> authService.validateToken("otherToken")
        );
    }

}
