package ru.t1.karimov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.api.repository.IUserRepository;
import ru.t1.karimov.tm.api.service.*;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.enumerated.TaskSort;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.entity.TaskNotFoundException;
import ru.t1.karimov.tm.exception.field.*;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.repository.ProjectRepository;
import ru.t1.karimov.tm.repository.TaskRepository;
import ru.t1.karimov.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@Category(UnitCategory.class)
public class TaskServiceTest {

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IUserService userService;

    @NotNull
    private IProjectTaskService projectTaskService;

    @NotNull
    private List<Task> taskList;

    @NotNull
    private List<User> userList;

    @Before
    public void initTest() throws AbstractException {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        projectService = new ProjectService(projectRepository);
        taskService = new TaskService(taskRepository);
        projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
        userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);
        projectList = new ArrayList<>();
        taskList = new ArrayList<>();
        userList = new ArrayList<>();

        @NotNull final User userAdmin = new User();
        userAdmin.setLogin("admin");
        userAdmin.setPasswordHash("admin");
        userAdmin.setEmail("admin@tst.ru");
        userAdmin.setRole(Role.ADMIN);
        @NotNull final User userTest = new User();
        userTest.setLogin("test");
        userTest.setPasswordHash("test");
        userTest.setEmail("test@tst.ru");
        userTest.setRole(Role.USUAL);
        @NotNull final String adminId = userAdmin.getId();
        @NotNull final String userId = userTest.getId();

        userService.add(userAdmin);
        userService.add(userTest);
        userList.add(userAdmin);
        userList.add(userTest);
        projectList.add(new Project(adminId, "Project 1", "Desc 1", Status.IN_PROGRESS));
        projectList.add(new Project(adminId, "Project 2", "Desc 2", Status.NOT_STARTED));
        projectList.add(new Project(userId, "Project 1", "Desc 1", Status.IN_PROGRESS));
        projectList.add(new Project(userId, "Project 2", "Desc 2", Status.IN_PROGRESS));
        projectList.add(new Project(userId, "Project 3", "Desc 3", Status.NOT_STARTED));
        taskList.add(new Task(adminId, "Task 1", "Desc 1", Status.NOT_STARTED, projectList.get(0).getId()));
        taskList.add(new Task(adminId, "Task 2", "Desc 2", Status.IN_PROGRESS, projectList.get(0).getId()));
        taskList.add(new Task(userId, "Task 1", "Desc 1", Status.IN_PROGRESS, projectList.get(4).getId()));
        taskList.add(new Task(userId, "Task 2", "Desc 2", Status.IN_PROGRESS, null));
        for (@NotNull final Project project : projectList) projectService.add(project);
        for (@NotNull final Task task : taskList) taskService.add(task);
    }

    @Test
    public void testAdd() throws AbstractException {
        @NotNull final Task task = new Task("test", "Task", "Description", Status.NOT_STARTED, "");
        taskService.add(task);
        final int expectedCount = taskList.size() + 1;
        assertEquals(expectedCount, taskService.getSize());
    }

    @Test
    public void testAddAll() {
        @NotNull List<Task> tasks = new ArrayList<>();
        final int expectedCount = taskList.size();
        for (int i = 0; i < expectedCount; i++) {
            tasks.add(new Task());
        }
        taskService.add(tasks);
        assertEquals(expectedCount * 2, taskService.getSize());
    }

    @Test
    public void testCreate() throws AbstractException {
        int expectedSize = taskList.size();
        int i = 0;
        for(@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final String name = "Task " + user.getLogin();
            @NotNull final Task task = taskService.create(userId, name);
            assertNotNull(task);
            i++;
        }
        expectedSize = expectedSize + i;
        assertEquals(expectedSize, taskService.getSize());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateWithNullUser() throws AbstractException {
        assertNotNull(taskService.create(null, "Task name"));
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateWithNullName() throws AbstractException {
        assertNotNull(taskService.create("test", null));
    }

    @Test
    public void testCreateFull() throws AbstractException {
        int expectedSize = taskList.size();
        int i = 0;
        for(@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final String name = "Task " + user.getLogin();
            @NotNull final String description = "Description " + user.getLogin();
            @NotNull final Task task = taskService.create(userId, name, description);
            assertNotNull(task);
            i++;
        }
        expectedSize = expectedSize + i;
        assertEquals(expectedSize, taskService.getSize());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateFullWithNullUser() throws AbstractException {
        assertNotNull(taskService.create(null, "Task name", "Desc"));
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateFullWithNullName() throws AbstractException {
        assertNotNull(taskService.create("test", null, "Desc"));
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testCreateFullWithNullDesc() throws AbstractException {
        assertNotNull(taskService.create("test", "Task name", null));
    }

    @Test
    public void testClear() {
        taskService.removeAll();
        assertEquals(0, taskService.getSize());
    }

    @Test
    public void testClearForUserPositive() throws AbstractException {
        for(@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            taskService.removeAll(userId);
            assertEquals(0, taskService.getSize(userId));
        }
        assertEquals(0, taskService.getSize());
    }

    @Test
    public void testClearForUserNegative() throws AbstractException {
        @NotNull final String otherUserId = "OtherUserId";
        taskService.removeAll(otherUserId);
        assertEquals(taskList.size(), taskService.getSize());
    }

    @Test
    public void testChangeStatusByIdPositive() throws AbstractException {
        for (@NotNull final Task task : taskList) {
            @Nullable final String userId = task.getUserId();
            @NotNull final String id = task.getId();
            assertNotNull(taskService.changeTaskStatusById(userId, id, Status.COMPLETED));
            @Nullable final Task actualTask = taskService.findOneById(userId, id);
            assertNotNull(actualTask);
            @NotNull final Status actualStatus = actualTask.getStatus();
            assertEquals(Status.COMPLETED, actualStatus);
        }
    }

    @Test(expected = TaskNotFoundException.class)
    public void testChangeStatusByIdNegative() throws AbstractException {
        @NotNull final String userId = "otherUserId";
        @NotNull final String id = "otherId";
        assertNotNull(taskService.changeTaskStatusById(userId, id, Status.COMPLETED));
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeStatusByIdNullStatus() throws AbstractException {
        @Nullable final String userId = taskList.get(2).getUserId();
        @NotNull final String id = taskList.get(2).getId();
        assertNotNull(taskService.changeTaskStatusById(userId, id, null));
    }

    @Test
    public void testChangeStatusByIndexPositive() throws AbstractException {
        for(@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Task> tasks = taskList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (int i = 0; i < tasks.size(); i++) {
                assertNotNull(taskService.changeTaskStatusByIndex(userId, i, Status.COMPLETED));
                @Nullable final Task actualTask = taskService.findOneByIndex(userId, i);
                assertNotNull(actualTask);
                @NotNull final Status actualStatus = actualTask.getStatus();
                assertEquals(Status.COMPLETED, actualStatus);
            }
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeStatusByIndexNegative() throws AbstractException {
        @NotNull final String userId = "otherUserId";
        final int index = taskList.size() + 1;
        assertNotNull(taskService.changeTaskStatusByIndex(userId, index, Status.COMPLETED));
        @Nullable final Task actualTask = taskService.findOneByIndex(userId, index);
        assertNull(actualTask);
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeStatusByIndexNullStatus() throws AbstractException {
        @NotNull final String userId = userList.get(1).getId();
        final int index = 0;
        assertNotNull(taskService.changeTaskStatusByIndex(userId, index, null));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Task> tasks = taskService.findAll();
        assertNotNull(tasks);
        assertEquals(taskList.size(), tasks.size());
    }

    @Test
    public void testFindAllUser() throws AbstractException {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Task> actualTasks = taskService.findAll(userId);
            @NotNull final List<Task> expectedTasks = taskList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            assertEquals(expectedTasks, actualTasks);
        }
    }

    @Test
    public void testFindAllSort() {
        for (@NotNull final TaskSort sort : TaskSort.values()) {
            @NotNull final Comparator<Task> comparator = sort.getComparator();
            @NotNull final List<Task> actualTasks = taskService.findAll(comparator);
            assertNotNull(actualTasks);
            @NotNull final List<Task> expectedTasks = taskList.stream()
                    .sorted(comparator)
                    .collect(Collectors.toList());
            assertEquals(expectedTasks, actualTasks);
        }
    }

    @Test
    public void testFindAllForUserSort() throws AbstractException {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            for (@NotNull final TaskSort sort : TaskSort.values()) {
                @NotNull final Comparator<Task> comparator = sort.getComparator();
                @NotNull final List<Task> actualTasks = taskService.findAll(userId, comparator);
                assertNotNull(actualTasks);
                @NotNull final List<Task> expectedTasks = taskList.stream()
                        .filter(m -> userId.equals(m.getUserId()))
                        .sorted(comparator)
                        .collect(Collectors.toList());
                assertEquals(expectedTasks, actualTasks);
            }
        }
    }

    @Test
    public void testFindByIdPositive() throws AbstractException {
        for (@NotNull final Task task : taskList) {
            @NotNull final String id = task.getId();
            assertEquals(task, taskService.findOneById(id));
        }
    }

    @Test
    public void testFindByIdNegative() throws AbstractException {
        @NotNull final String id = "OtherId";
        @Nullable final Task task = taskService.findOneById(id);
        assertNull(task);
    }

    @Test
    public void testFindByIdUserPositive() throws AbstractException {
        for (@NotNull final Task task : taskList) {
            @NotNull final String id = task.getId();
            @Nullable final String userId = task.getUserId();
            assertEquals(task, taskService.findOneById(userId, id));
        }
    }

    @Test
    public void testFindByIdForUserNegative() throws AbstractException {
        @NotNull final String id = "OtherId";
        @NotNull final String userId = "OtherUserId";
        @Nullable final Task task = taskService.findOneById(userId, id);
        assertNull(task);
    }

    @Test
    public void testFindByIndexPositive() throws AbstractException {
        for (int i = 0; i < taskList.size(); i++) {
            @Nullable final Task actualTask = taskService.findOneByIndex(i);
            @Nullable final Task expectedTask = taskList.get(i);
            assertNotNull(actualTask);
            assertEquals(expectedTask, actualTask);
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexNegative() throws AbstractException {
        final int index = taskList.size() + 1;
        assertNull(taskService.findOneByIndex(index));
    }

    @Test
    public void testFindByIndexForUserPositive() throws AbstractException {
        for(@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Task> tasks = taskList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (int i = 0; i < tasks.size(); i++) {
                assertNotNull(taskService.findOneByIndex(userId, i));
                @Nullable final Task actualTask = taskService.findOneByIndex(userId, i);
                assertNotNull(actualTask);
                @Nullable final Task expectedTask = tasks.get(i);
                assertEquals(expectedTask, actualTask);
            }
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexForUserNegative() throws AbstractException {
        @NotNull final String userId = "OtherUserId";
        final int index = taskList.size() + 1;
        assertNull(taskService.findOneByIndex(userId, index));
    }

    @Test
    public void testFindByProjectId() throws AbstractException {
        for (@NotNull final Task task : taskList) {
            @Nullable final String userId = task.getUserId();
            @Nullable final String projectId = task.getProjectId();
            if (userId == null || projectId == null) continue;
            @NotNull final List<Task> actualList = taskService.findAllByProjectId(userId, projectId);
            @NotNull final List<Task> expectedList = taskList.stream()
                    .filter(m -> m.getProjectId() != null)
                    .filter(m -> userId.equals(m.getUserId()))
                    .filter(m -> projectId.equals(m.getProjectId()))
                    .collect(Collectors.toList());
            assertEquals(expectedList, actualList);
            assertEquals(expectedList.size(), actualList.size());
        }
    }

    @Test
    public void testGetSize() {
        final int actualTaskSize = taskService.getSize();
        assertEquals(taskList.size(), actualTaskSize);
    }

    @Test
    public void testGetSizeUser() throws AbstractException {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            final int actualTaskSize = taskService.getSize(userId);
            @NotNull final List<Task> expectedTasks = taskList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            final int expectedTaskSize = expectedTasks.size();
            assertEquals(expectedTaskSize, actualTaskSize);
        }
    }

    @Test
    public void testGetSizeUserNegative() throws AbstractException {
        @NotNull final String userId = "OtherUserId";
        final int actualTaskSize = taskService.getSize(userId);
        assertEquals(0, actualTaskSize);
    }

    @Test
    public void testExistsById() {
        for (@NotNull final Task task : taskList) {
            @NotNull final String id = task.getId();
            assertTrue(taskService.existsById(id));
        }
        assertFalse(taskService.existsById("OtherId"));
    }

    @Test
    public void testExistsByEmptyIdNegative() {
        assertFalse(taskService.existsById(""));
    }

    @Test
    public void testExistsByIdForUser() throws AbstractException {
        for (@NotNull final Task task : taskList) {
            @NotNull final String id = task.getId();
            @Nullable final String userId = task.getUserId();
            assertTrue(taskService.existsById(userId, id));
        }
        assertFalse(taskService.existsById("OtherUserId","OtherId"));
    }

    @Test(expected = IdEmptyException.class)
    public void testExistsByIdForUserWithNullId() throws AbstractException {
        assertFalse(taskService.existsById(userList.get(0).getId(),null));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testExistsByIdForUserWithNullUserId() throws AbstractException {
        assertFalse(taskService.existsById(null, taskList.get(0).getId()));
    }

    @Test
    public void testRemovedAllForUserPositive() throws AbstractException {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            taskService.removeAll(userId);
            assertEquals(0, taskService.getSize(userId));
        }
        assertEquals(0, taskService.getSize());
    }

    @Test
    public void testRemovedAllForUserNegative() throws AbstractException {
        @NotNull final String userId = "OtherUserId";
        taskService.removeAll(userId);
        assertEquals(taskList.size(), taskService.getSize());
    }

    @Test
    public void testRemovedByIdPositive() throws AbstractException {
        for (@NotNull final Task task : taskList) {
            @NotNull final String id = task.getId();
            assertNotNull(taskService.removeOneById(id));
        }
        assertEquals(0, taskService.getSize());
    }

    @Test
    public void testRemovedByIdNegative() throws AbstractException {
        @NotNull final String id = "OtherId";
        assertNull(taskService.removeOneById(id));
        assertEquals(taskList.size(), taskService.getSize());
    }

    @Test(expected = IdEmptyException.class)
    public void testRemovedByEmptyIdNegative() throws AbstractException {
        @NotNull final String id = "";
        assertNull(taskService.removeOneById(id));
        assertEquals(taskList.size(), taskService.getSize());
    }

    @Test
    public void testRemoveByIdForUserPositive() throws AbstractException {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Task> tasks = taskList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (@NotNull final Task task : tasks) {
                @NotNull final String id = task.getId();
                assertNotNull(taskService.removeOneById(userId, id));
            }
            assertEquals(0, taskService.getSize(userId));
        }
        assertEquals(0, taskService.getSize());
    }

    @Test
    public void testRemovedByIdForUserNegative() throws AbstractException {
        @NotNull final String id = "OtherId";
        @NotNull final String userId = "OtherUserId";
        assertNull(taskService.removeOneById(userId, id));
        assertEquals(taskList.size(), taskService.getSize());
    }

    @Test(expected = IdEmptyException.class)
    public void testRemovedByEmptyIdForUserNegative() throws AbstractException {
        @NotNull final String id = "";
        @NotNull final String userId = userList.get(0).getId();
        assertNull(taskService.removeOneById(userId, id));
    }

    @Test(expected = IdEmptyException.class)
    public void testRemovedByNullIdForUserNegative() throws AbstractException {
        @NotNull final String userId = userList.get(0).getId();
        assertNull(taskService.removeOneById(userId, null));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemovedByIdForUserWithNullUserIdNegative() throws AbstractException {
        @NotNull final String id = taskList.get(0).getId();
        assertNull(taskService.removeOneById(null, id));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemovedByIdForUserWithEmptyUserIdNegative() throws AbstractException {
        @NotNull final String id = taskList.get(0).getId();;
        @NotNull final String userId = "";
        assertNull(taskService.removeOneById(userId, id));
    }

    @Test
    public void testRemoveByIndexPositive() throws AbstractException {
        for (int i = 0; i < taskList.size(); i++) {
            assertNotNull(taskService.removeOneByIndex(0));
        }
        assertEquals(0, taskService.getSize());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIndexNegative() throws AbstractException {
        @NotNull final Integer index = taskList.size() + 1;
        assertNull(taskService.removeOneByIndex(index));
    }

    @Test
    public void testRemoveByIndexForUserPositive() throws AbstractException {
        for(@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Task> tasks = taskList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (int i = 0; i < tasks.size(); i++) {
                assertNotNull(taskService.removeOneByIndex(userId, 0));
            }
            assertEquals(0, taskService.getSize(userId));
        }
        assertEquals(0, taskService.getSize());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIndexForUserNegative() throws AbstractException {
        @NotNull final String userId = "otherUserId";
        @NotNull final Integer index = taskList.size() + 1;
        assertNull(taskService.removeOneByIndex(userId, index));
    }

    @Test
    public void testUpdateByIdPositive() throws AbstractException {
        for (@NotNull final Task task : taskList) {
            @Nullable final String userId = task.getUserId();
            if (userId == null) continue;
            @NotNull final String id = task.getId();
            @NotNull final String name = "Task " + id;
            @NotNull final String description = "Desc " + id;
            @Nullable final Task actualTask = taskService.updateTaskById(userId, id, name, description);
            assertEquals(name, actualTask.getName());
            assertEquals(description, actualTask.getDescription());
            assertEquals(task, actualTask);
        }
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUpdateByIdOtherUserIdNegative() throws AbstractException {
        @NotNull final String userId = "OtherUserId";
        @NotNull final String id = taskList.get(0).getId();
        @NotNull final String name = "Task " + id;
        @NotNull final String description = "Desc " + id;
        assertNotNull(taskService.updateTaskById(userId, id, name, description));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIdNullUserIdNegative() throws AbstractException {
        @NotNull final String id = taskList.get(0).getId();
        @NotNull final String name = "Task " + id;
        @NotNull final String description = "Desc " + id;
        assertNotNull(taskService.updateTaskById(null, id, name, description));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIdEmptyUserIdNegative() throws AbstractException {
        @NotNull final String id = taskList.get(0).getId();
        @NotNull final String name = "Task " + id;
        @NotNull final String description = "Desc " + id;
        assertNotNull(taskService.updateTaskById("", id, name, description));
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateByNullIdNegative() throws AbstractException {
        @Nullable final String userId = taskList.get(0).getUserId();
        @NotNull final String name = "Task ";
        @NotNull final String description = "Desc ";
        assertNotNull(taskService.updateTaskById(userId, null, name, description));
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateByEmptyIdNegative() throws AbstractException {
        @NotNull final String id = "";
        @Nullable final String userId = taskList.get(0).getUserId();
        @NotNull final String name = "Task " + id;
        @NotNull final String description = "Desc " + id;
        assertNotNull(taskService.updateTaskById(userId, id, name, description));
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIdNullNameNegative() throws AbstractException {
        @NotNull final String id = taskList.get(0).getId();
        @Nullable final String userId = taskList.get(0).getUserId();
        @NotNull final String description = "Desc " + id;
        assertNotNull(taskService.updateTaskById(userId, id, null, description));
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIdEmptyNameNegative() throws AbstractException {
        @NotNull final String id = taskList.get(0).getId();
        @Nullable final String userId = taskList.get(0).getUserId();
        @NotNull final String name = "";
        @NotNull final String description = "Desc " + id;
        assertNotNull(taskService.updateTaskById(userId, id, name, description));
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testUpdateByIdNullDescNegative() throws AbstractException {
        @NotNull final String id = taskList.get(0).getId();
        @Nullable final String userId = taskList.get(0).getUserId();
        @NotNull final String name = "Task " + id;
        assertNotNull(taskService.updateTaskById(userId, id, name, null));
    }

    @Test
    public void testUpdateByIndexPositive() throws AbstractException {
        for(@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Task> tasks = taskList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (int i = 0; i < tasks.size(); i++) {
                @NotNull final String name = "Task new " + i;
                @NotNull final String description = "Desc new " + i;
                assertNotNull(taskService.findOneByIndex(userId, i));
                @Nullable final Task actualTask = taskService.updateTaskByIndex(userId, i, name, description);
                assertEquals(name, actualTask.getName());
                assertEquals(description, actualTask.getDescription());
                assertEquals(tasks.get(i), actualTask);
            }
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndexNegative() throws AbstractException {
        @NotNull final String userId = "OtherUserId";
        final int index = taskList.size() + 1;
        @NotNull final String name = "Task new " + index;
        @NotNull final String description = "Desc new " + index;
        assertNotNull(taskService.updateTaskByIndex(userId, index, name, description));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByNullIndexNegative() throws AbstractException {
        @NotNull final String userId = userList.get(0).getId();
        final int index = taskList.size() + 1;
        @NotNull final String name = "Task new " + index;
        @NotNull final String description = "Desc new " + index;
        assertNotNull(taskService.updateTaskByIndex(userId, null, name, description));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByOutIndexNegative() throws AbstractException {
        @NotNull final String userId = userList.get(0).getId();
        final int index = taskList.size() + 1;
        @NotNull final String name = "Task new " + index;
        @NotNull final String description = "Desc new " + index;
        assertNotNull(taskService.updateTaskByIndex(userId, index, name, description));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndexOtherUserIdNegative() throws AbstractException {
        @NotNull final String userId = "OtherUserId";
        final int index = 0;
        @NotNull final String name = "Task new " + index;
        @NotNull final String description = "Desc new " + index;
        assertNotNull(taskService.updateTaskByIndex(userId, index, name, description));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIndexNullUserIdNegative() throws AbstractException {
        final int index = 0;
        @NotNull final String name = "Task new " + index;
        @NotNull final String description = "Desc new " + index;
        assertNotNull(taskService.updateTaskByIndex(null, index, name, description));
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIndexNullNameNegative() throws AbstractException {
        @NotNull final String userId = userList.get(0).getId();
        final int index = 0;
        @NotNull final String description = "Desc new " + index;
        assertNotNull(taskService.updateTaskByIndex(userId, index, null, description));
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIndexEmptyNameNegative() throws AbstractException {
        @NotNull final String userId = userList.get(0).getId();
        final int index = 0;
        @NotNull final String name = "";
        @NotNull final String description = "Desc new " + index;
        assertNotNull(taskService.updateTaskByIndex(userId, index, name, description));
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testUpdateByIndexNullDescNegative() throws AbstractException {
        @NotNull final String userId = userList.get(0).getId();
        final int index = 0;
        @NotNull final String name = "Name new " + index;
        assertNotNull(taskService.updateTaskByIndex(userId, index, name, null));
    }

}
