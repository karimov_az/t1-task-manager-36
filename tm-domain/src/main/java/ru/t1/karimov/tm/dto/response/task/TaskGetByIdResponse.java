package ru.t1.karimov.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.response.AbstractTaskResponse;
import ru.t1.karimov.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public final class TaskGetByIdResponse extends AbstractTaskResponse {

    public TaskGetByIdResponse(@Nullable final Task task) {
        super(task);
    }

}
