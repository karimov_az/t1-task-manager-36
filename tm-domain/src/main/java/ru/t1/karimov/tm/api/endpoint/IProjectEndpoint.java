package ru.t1.karimov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.dto.request.project.*;
import ru.t1.karimov.tm.dto.response.project.*;
import ru.t1.karimov.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface IProjectEndpoint extends IEndpoint {

    @NotNull
    String NAME = "ProjectEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IProjectEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IProjectEndpoint.class);
    }

    @NotNull
    @WebMethod
    ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull ProjectChangeStatusByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectClearResponse clearProjects(@NotNull ProjectClearRequest request) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectCompleteByIdResponse completeProjectById(@NotNull ProjectCompleteByIdRequest request) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectCompleteByIndexResponse completeProjectByIndex(@NotNull ProjectCompleteByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectCreateResponse createProject(@NotNull ProjectCreateRequest request) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectGetByIdResponse getProjectById(@NotNull ProjectGetByIdRequest request) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectGetByIndexResponse getProjectByIndex(@NotNull ProjectGetByIndexRequest request) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectListResponse listProject(@NotNull ProjectListRequest request) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectRemoveByIdResponse removeProjectById(@NotNull ProjectRemoveByIdRequest request) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull ProjectRemoveByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectStartByIdResponse startProjectById(@NotNull ProjectStartByIdRequest request) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectStartByIndexResponse startProjectByIndex(@NotNull ProjectStartByIndexRequest request) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectUpdateByIdResponse updateProjectById(@NotNull ProjectUpdateByIdRequest request) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull ProjectUpdateByIndexRequest request
    ) throws AbstractException;

}
