package ru.t1.karimov.tm.dto.request.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class TaskClearRequest extends AbstractUserRequest {

    public TaskClearRequest(@Nullable final String token) {
        super(token);
    }

}
